#!/usr/bin/env python3

import argparse
import subprocess
import os
import sys
# import pandas as pd
from yaml import dump as yaml_dump
from yaml import safe_load as yaml_load


def get_version():
    version = open(os.path.join(repo_path, 'VERSION'), 'r').readline().strip()
    return version


def get_gitversion(fixed_version: str):
    cwd = os.getcwd()
    os.chdir(repo_path)
    try:
        git_hash = subprocess.check_output(["git", "describe", "--always"], stderr = subprocess.DEVNULL).decode('ascii').strip()
    except:
        git_hash = fixed_version
    os.chdir(cwd)
    return git_hash


def check_sample_list(sample_list):
    # Check whether sample list has header
    type2 = "sample\tlong\tshort1\tshort2\n"

    with open(sample_list, 'r') as handle:
        header_line = handle.readline()

    if header_line != type2:
        print(f"ERROR: sample list {sample_list} does not have a proper header. Make sure that first line is \nsample\tlong\tshort1\tshort2\n(tab-separated)")
        sys.exit(1)


def create_config(configfile, args):
    #region # checks

    if not os.path.exists(args.kraken2db):
        print(f"ERROR: path to kraken2 database {args.kraken2db} does not exist")
        sys.exit(1)

    if not os.path.exists(args.taxonkit_db):
        print(f"ERROR: path to taxonkit_db database {args.taxonkit_db} does not exist")
        sys.exit(1)

        if not os.path.exists(args.platon_db):
            print(f"ERROR: path to platon db {args.platon_db}  does not exist")
            sys.exit(1)

    # if not os.path.exists(args.mash_plasmid_db):
    #     print(f"ERROR: path {args.mash_plasmid_db} does not exist")
    #     sys.exit(1)
    #
    # if not os.path.exists(args.mash_genome_db):
    #     print(f"ERROR: path {args.mash_genome_db} does not exist")
    #     sys.exit(1)
    #
    # if not os.path.exists(args.mash_genome_info):
    #     print(f"ERROR: path {args.mash_genome_info} does not exist")
    #     sys.exit(1)

    # check if sample list has proper format
    check_sample_list(args.sample_list)

    #endregion # checks
    #region # settings

    # define do_pilon
    if args.no_pilon:
        do_pilon = False
    else:
        do_pilon = True

    if args.no_long and not args.no_flye:
        print("ERROR: You chose --no-long but not --no-flye")
        sys.exit(1)

    # define ASSEMBLER
    if args.no_unicycler and not args.no_flye:
        print("Only flye")
        ASSEMBLER = ["flye"]
    elif not args.no_unicycler and args.no_flye:
        print("Only unicycler")
        ASSEMBLER = ["unicycler"]
        # implies no pilon
        do_pilon = False
    elif not args.no_unicycler and not args.no_flye:
        print("flye and unicycler")
        ASSEMBLER = ["unicycler", "flye"]
    else:
        print("Choose at least one assembler")
        sys.exit(1)

    # define READTYPE
    if args.no_short:
        print("Not using short reads")
        READTYPE = ["longreads"]
        # implies no pilon
        do_pilon = False
    elif args.no_long:
        READTYPE = ["shortreads"]
    else:
        READTYPE = ["shortreads", "longreads"]
        do_pilon = not args.no_pilon

    if args.no_flye:
        do_pilon = False

    # define APPLIED_DATABASES
    APPLIED_DATABASES = args.abricate_databases  # check existence

    if args.pacbio:
        technology = "PacBio"
        if args.flye_read_type == "--nano-raw":
            print('When using flag "--pacbio", please also change "flye_read-type" to \'--pacbio-raw\'   \'--pacbio-corr\' \'--pacbio-hifi\' ')
            sys.exit(1)
    elif args.no_long:
        print("Only performing short read assembly!")
        technology = "Illumina"
    else:
        technology = "ONT"

    #endregion # settings
    #region # create config dictionary

    config_dict = {
        'pipeline'   : pipeline,
        'version'    : version,
        'git_version': git_version,
        'workdir'    : args.working_directory,
        'samples'    : args.sample_list,
        'assembler'  : ASSEMBLER,
        'smk_params' : {
            'log_directory': args.logdir
        },
        'params'     : {
            'threads'   : args.threads_sample,
            'readtype'  : READTYPE,
            'technology': technology,
            'trimming'  : {
                'do_trimming': not args.no_trimming,
                'minscore'   : args.trimming_minscore
            },
            'filtering' : {
                'do_filtering': not args.no_filtering,
                'minquality'  : args.filtering_minquality,
                'minlength'   : args.filtering_minlength,
                'extra'       : args.filtering_extra
            },
            'unicycler' : {
                'do_unicycler': not args.no_unicycler,
                'mode'        : args.unicycler_mode,
                'keep'        : args.unicycler_keep,
                'extra'       : args.unicycler_extra
            },
            'flye'      : {
                'do_flye'   : not args.no_flye,
                'read_type' : args.flye_read_type,
                'iterations': args.flye_iterations,
                'extra'     : args.flye_extra
            },
            'abricate'  : {
                'applied_databases': APPLIED_DATABASES,
                'path'             : args.abricate_db,
                'minid'            : args.abricate_minid,
                'mincov'           : args.abricate_mincov,
                'mincov_contigends': args.abricate_mincov_contigends
            },
            'platon'    : {
                'db': args.platon_db
            },
            'pilon'     : {
                'do_pilon'  : do_pilon,
                'iterations': args.pilon_iterations,
                'fix'       : args.pilon_fix,
                'extra'     : args.pilon_extra
            },
            'kraken2'   : {
                'db_kraken'        : args.kraken2db,
                'taxonkit_db'      : args.taxonkit_db,
                'taxonkit_taxlevel': args.taxlevel,
                'do_kraken'        : True
            }
        }
    }
    # endregion # create config dictionary

    # write dictionary to yaml
    with open(configfile, 'w') as outfile:
        yaml_dump(config_dict, outfile, default_flow_style = False, sort_keys = False)
    print(f"Config written to {configfile}")


def run_snakemake(configfile, args):
    # force mode
    force_mode = "--forceall" if args.forceall else ""

    # other workflow options
    dryrun = "--dryrun --quiet" if args.dryrun else ""
    unlock = "--unlock" if args.unlock else ""

    call = f"snakemake --configfile {configfile} --snakefile {args.snakefile} {force_mode} --cores {args.threads} --keep-going -p {dryrun} {unlock}"

    print(call)
    subprocess.call(call, shell = True, cwd = args.logdir)


def main():
    # define global vars
    global pipeline
    global repo_path
    global db_path

    # set global vars
    pipeline = "MiLongA"
    repo_path = os.path.dirname(os.path.realpath(__file__))
    db_path = os.path.join(os.environ['CONDA_PREFIX'], "db")

    #region # parse arguments
    parser = argparse.ArgumentParser()

    # path arguments
    parser.add_argument('-l', '--sample_list', help = 'List of samples to analyze, as a two column tsv file with columns sample and assembly. Can be generated with provided script create_sampleSheet,sh', default = "/", type = os.path.abspath, required = False)
    parser.add_argument('-d', '--working_directory', help = 'Working directory where results are saved', default = "/", type = os.path.abspath, required = False)
    parser.add_argument('-c', '--config', help = 'Path to pre-defined config file (Other parameters will be ignored)', default = "/", type = os.path.abspath, required = False)
    parser.add_argument('-s', '--snakefile', help = 'Path to Snakefile of long read assembly pipeline, default is path to Snakefile in same directory', default = os.path.join(repo_path, "pipeline.smk"), required = False)

    # general parameters
    parser.add_argument('--no-trimming', help = 'Do not trim long reads', default = False, action = 'store_true', required = False)
    parser.add_argument('--no-filtering', help = 'Do not perform quality and length filtering on long reads', default = False, action = 'store_true', required = False)
    parser.add_argument('--no-unicycler', help = 'Do not Perform unicycler assembly', default = False, action = 'store_true', required = False)
    parser.add_argument('--no-flye', help = 'Do not Perform flye assembly', default = False, action = 'store_true', required = False)
    parser.add_argument('--no-pilon', help = 'Do not Perform pilon polising on flye assembly', default = False, action = 'store_true', required = False)
    parser.add_argument('--no-short', help = 'Do not Perform mapping with short reads', default = False, action = 'store_true', required = False)
    parser.add_argument('--no-long', help = 'Do only perorm unicycler assembly, with short reads only', default = False, action = 'store_true', required = False)
    parser.add_argument('--pacbio', help = 'Long reads are pacbio reads. Do not run Nanostat', default = False, action = 'store_true', required = False)

    # trimming
    parser.add_argument('--trimming_minscore', help = 'Minimum score to keep reads in trimming, default=60', default = 60, required = False)

    # filtering
    do_filtering: true
    parser.add_argument('--filtering_minquality', help = 'Minimum quality score of reads to keep reads in filtering, default=7', default = 7, required = False)
    parser.add_argument('--filtering_minlength', help = 'Minimum length to keep reads in trimming, default=1000', default = 1000, required = False)
    parser.add_argument('--filtering_extra', help = 'Extra parameters for filtering, default=""', default = "", required = False)

    # unicycler
    parser.add_argument('--unicycler_mode', help = 'unicycler mode option: Choose from conservative,normal,bold, default="normal"', default = "normal", required = False)
    parser.add_argument('--unicycler_keep', help = 'unicycler keep option:  choose 0 or 1, see unicycler --help for more info, default=0', default = 0, required = False)
    parser.add_argument('--unicycler_extra', help = 'Extra parameters for unicycler, enclose in \', default=""', default = "", required = False)

    # flye
    parser.add_argument('--flye_read_type', help = 'flye read type: choose from \'--pacbio-raw\'   \'--pacbio-corr\'   \'--pacbio-hifi\'   \'--nano-raw\'   \'--nano-corr\'   \'--subassemblies\', default="--nano-raw"', default = "--nano-raw", required = False)
    parser.add_argument('--flye_iterations', help = 'Number of self-polising iterations in flye, default=3', default = 3, required = False)
    parser.add_argument('--flye_extra', help = 'Extra parameters for flye, enclose in \', default=""', default = "", required = False)

    # pilon
    parser.add_argument('--pilon_fix', help = 'Type of errors to fix with pilon: choose from snps,indels,gaps,local,all,bases,none, default="all"', default = "all", required = False)
    parser.add_argument('--pilon_iterations', help = 'Number of polishing iterations in pilon, default=5', default = 5, required = False)
    parser.add_argument('--pilon_extra', help = 'Extra parameters for pilon, enclose in \', default=""', default = "", required = False)

    # abricate parameters
    parser.add_argument('--abricate_databases', help = 'List of databases to search for genes, default=["ncbi","plasmidfinder"]', default = ["ncbi", "plasmidfinder"], required = False)
    parser.add_argument('--abricate_minid', help = 'Minimum identity in abricate, default=80', default = 80, required = False)
    parser.add_argument('--abricate_mincov', help = 'Minimum coverage in abricate, default=80', default = 80, required = False)
    parser.add_argument('--abricate_mincov_contigends', help = 'Minimum coverage in abricate, default=20', default = 80, required = False)
    parser.add_argument('--abricate_db', help = 'Path to abricate and prokka databases, default is database of conda environment', default = db_path, type = os.path.abspath, required = False)

    # platon
    parser.add_argument('--platon_db', help = f'Path to location of platon database for plasmid prediction: plasmid_db; default: {os.path.join(repo_path, "databases", "platon")}', default = os.path.join(repo_path, "databases/platon"), type = os.path.abspath, required = False)

    # extra
    parser.add_argument('--kraken2db', help = f'Path to kraken2 database; default: {os.path.join(repo_path, "databases", "kraken2")}', default = os.path.join(repo_path, "databases", "kraken2"), type = os.path.abspath, required = False)  # Note: minikraken2.tar.gz extracts to "../kraken/" not kraken2
    parser.add_argument('--taxonkit_db', help = f'Path to taxonkit_db; default: {os.path.join(repo_path, "databases", "taxonkit")}', default = os.path.join(repo_path, "databases", "taxonkit"), type = os.path.abspath, required = False)
    parser.add_argument('--taxlevel', help = 'taxonomic level for kraken2 contig parsing, default="species"', default = "species", required = False)

    # mash databases
    # parser.add_argument('--mash_genome_db', help='Path to mash db (default: path/2/repo/databases/mash/refseq.genomes.k21s1000.msh)', default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "databases/mash/refseq.genomes.k21s1000.msh"),required=False, type=os.path.abspath)
    # parser.add_argument('--mash_genome_info', help='Path to mash db (default: path/2/repo/databases/mash/refseq.genomes.k21s1000.info)', default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "databases/mash/refseq.genomes.k21s1000.info"),required=False, type=os.path.abspath)
    # parser.add_argument('--mash_plasmid_db', help='Path to mash db (default: path/2/repo/databases/mash/refseq.plasmid.k21s1000.msh)', default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "databases/mash/refseq.plasmid.k21s1000.msh"),required=False, type=os.path.abspath)

    # prokka parameters
    # parser.add_argument('--prokka', help='Also run prokka', default=False, action='store_true',  required=False)
    # parser.add_argument('--prokka_genus', help='Prokka genus option. See prokka --help for details. Default = "Genus"', default="Genus", required=False)

    # cpu arguments
    parser.add_argument('-t', '--threads', help = 'Number of Threads to use. Note that samples can only be processed sequentially due to the required database access. However the allele calling can be executed in parallel for the different loci, default = 10', default = 10, required = False)
    parser.add_argument('-n', '--dryrun', help = 'Snakemake dryrun. Only calculate graph without executing anything', default = False, action = 'store_true', required = False)
    parser.add_argument('--threads_sample', help = 'Number of Threads to use per sample, default = 10', default = 10, required = False, type = int)
    parser.add_argument('--forceall', help = 'Snakemake force. Force recalculation of all steps', default = False, action = 'store_true', required = False)
    parser.add_argument('--unlock', help = 'unlock snakemake', default = False, action = 'store_true', required = False)
    parser.add_argument('--logdir', help = 'Path to directory where .snakemake output is to be saved', default = "/", type = os.path.abspath, required = False)
    parser.add_argument('-V', '--version', help = 'Print program version', default = False, action = 'store_true', required = False)
    args = parser.parse_args()

    #endregion # parse arguments
    #region # eval arguments

    # git version
    global version
    global git_version
    version = get_version()
    git_version = get_gitversion(fixed_version = version)

    if args.version:
        print(f"{pipeline} version\t{version}")
        if not version == git_version:
            print(f"commit  version\t{git_version.lstrip('v')}")
        sys.exit(0)
    else:
        print(f"You are using {pipeline} version {version} ({git_version})")

    # checks
    if not os.path.exists(args.snakefile):
        print(f"ERROR: path to Snakefile {args.snakefile} does not exist")
        sys.exit(1)

    # write config if none exists
    if not args.config == "/" and os.path.exists(args.config):  # use pre-defined config
        configfile = args.config
        print(f"Loading pre-defined configfile: {configfile}")
        with open(configfile, "r") as stream:
            try:
                config_dict = yaml_load(stream)
            except Error as exc:
                print(f"ERROR: the configfile {configfile} could not be loaded. {exc}")
                sys.exit(1)
        args.logdir = config_dict['smk_params']['log_directory'] if config_dict['smk_params']['log_directory'] else args.logdir
    else:  # create new config
        # config checks
        if args.sample_list == "/":
            print("ERROR: the following argument is required: -l/--sample_list")
            sys.exit(1)
        elif not os.path.exists(args.sample_list):
            print(f"ERROR: path to sample list {args.sample_list} does not exist")
            sys.exit(1)
        if args.working_directory == "/":
            print("ERROR: the following argument is required: -d/--working_directory")
            sys.exit(1)
        elif not os.path.exists(os.path.dirname(args.working_directory)):
            print(f"ERROR: path to parent directory of working_directory {args.working_directory} does not exist")
            sys.exit(1)
        elif not os.path.exists(args.working_directory):
            os.makedirs(args.working_directory)
        args.logdir = args.working_directory if args.logdir == "/" else args.logdir
        configfile = os.path.join(args.working_directory, "config.yaml")  # TODO: rename to config_milonga.yaml
        print(f"Creating new config: {configfile}")
        create_config(configfile, args)

    #endregion # eval arguments

    if os.path.exists(configfile):
        print(f"Snakemake logs are saved in {args.logdir}")
        run_snakemake(configfile, args)
    else:
        print(f"ERROR: path to configfile {configfile} does not exist")
        sys.exit(1)


if __name__ == '__main__':
    main()
