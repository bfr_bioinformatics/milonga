# MiLongA: A snakemake workflow for Microbial Long-read Assembly



## Authors

* Carlus Deneke

## Summary

The pipeline allows assembly of long read data using different assemblers. 

Currently, unicycler and flye assemblers are supported.

From each generated assembly, various analyses are performed. These include:
* circularity
* plasmid prediction
* amr gene and plasmidfinder search
* long read and short read coverage per contig
* completeness and contamination analysis
* taxonomic classifcation
* dot plots of the created assemblies

All findings are summarized in an interactive html report.


### DAG of all rules


![DAG](dag.png)


## Installation

1. Clone this repository

```
git clone https://gitlab.com/bfr_bioinformatics/milonga
```

2. Create a conda environment for all software dependencies


Either
```
mamba env create -n milonga -f /path/to/repo/envs/milonga.yaml
```

or

```
conda env create -n milonga -f /path/to/repo/envs/milonga.yaml
```

_mamba_ is preferable over _conda_ if available.


NOTE that pilon's default RAM limit is not properly configured. To change this you must edit the file `pilon` in your installation, e.g. `$HOME/miniconda3/envs/milonga/share/pilon-1.24-0/pilon` (adjust path to conda and pilon version if necessary).
Change the entry for `default_jvm_mem_opts`:


From 

```
default_jvm_mem_opts = ['-Xms512m', '-Xmx1g']` 
```

to 

```
default_jvm_mem_opts = ['-Xms512m', '-Xmx20g']
```

This increases the maximal memory usage of pilon from 1 Gigabyte to 20 Gigabyte


3. Download additional databases

Please download the required databases and extract them under the folder /path/2/milonga/`databases`:

* [platon_db](https://zenodo.org/record/4066768/files/db.tar.gz)
* [minikraken2](https://genome-idx.s3.amazonaws.com/kraken/k2_standard_08gb_20220607.tar.gz)
* [taxdump](ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz)

You may also link existing kraken2 databases.

Also, you may also specify the location of each database in the milonga.py call (e.g. using `--platondb`) explitly

Alternatively, you may use the utility script `database_setup.sh` in the scripts directory

```
path/2/repo/scripts/milonga_setup.sh --databases --status
```


You may also use the setup script for a full setup.


```
path/2/repo/scripts/milonga_setup.sh --mamba --status --test_data --databases
```

See `milonga_setup.sh --help` for all options.

Important options are

* `--mamba`: automatically install all depdencies using mamba
* `--test_data`: Download a test data set
* `--databases`: Install all required databases



## Usage


### Creating a sample sheet

MiLongA requires a tab separated sample sheet with the following contents:

sample|long|short1|short2
---  | --- | --- | ---
sample1|link1_longreads|link1_shortreads_forward|link1_shortreads_reverse
sample2|link2_longreads|link2_shortreads_forward|link2_shortreads_reverse


The links to short reads are optional if option `--no-shorts` and `no--unicycler` is chosen.

### Using the wrapper

#### Simple usage

```
cd path/to/repo

./milonga.py --sample_list path/2/results/samples.tsv --working_directory path/2/results/

```

Important options

```
  -n, --dryrun          Snakemake dryrun
  -t THREADS, --threads THREADS
  --no-trimming         Do not trim long reads
  --no-filtering        Do not perform quality and length filtering on long reads
  --no-unicycler        Do not Perform unicycler assembly
  --no-flye             Do not Perform flye assembly
  --no-pilon            Do not Perform pilon polising on flye assembly
  --no-short            Do not Perform mapping with short reads
```

All available options are can be seen with

```
path/to/repo/milonga.py --help
```

#### Usage from existing configfile


```
path/to/repo/milonga.py --config config.yaml
```


#### All options

```
$ ./milonga.py --help
usage: milonga.py [-h] [-l SAMPLE_LIST] [-d WORKING_DIRECTORY] [-c CONFIG]
                  [-s SNAKEFILE] [--no-trimming] [--no-filtering]
                  [--no-unicycler] [--no-flye] [--no-pilon] [--no-short]
                  [--no-long] [--pacbio]
                  [--trimming_minscore TRIMMING_MINSCORE]
                  [--filtering_minquality FILTERING_MINQUALITY]
                  [--filtering_minlength FILTERING_MINLENGTH]
                  [--filtering_extra FILTERING_EXTRA]
                  [--unicycler_mode UNICYCLER_MODE]
                  [--unicycler_keep UNICYCLER_KEEP]
                  [--unicycler_extra UNICYCLER_EXTRA]
                  [--flye_read_type FLYE_READ_TYPE]
                  [--flye_iterations FLYE_ITERATIONS]
                  [--flye_extra FLYE_EXTRA] [--pilon_fix PILON_FIX]
                  [--pilon_iterations PILON_ITERATIONS]
                  [--pilon_extra PILON_EXTRA]
                  [--abricate_databases ABRICATE_DATABASES]
                  [--abricate_minid ABRICATE_MINID]
                  [--abricate_mincov ABRICATE_MINCOV]
                  [--abricate_mincov_contigends ABRICATE_MINCOV_CONTIGENDS]
                  [--abricate_db ABRICATE_DB] [--platon_db PLATON_DB]
                  [--kraken2db KRAKEN2DB] [--taxonkit_db TAXONKIT_DB]
                  [--taxlevel TAXLEVEL] [-t THREADS] [-n]
                  [--threads_sample THREADS_SAMPLE] [--forceall] [--unlock]
                  [--logdir LOGDIR]

optional arguments:
  -h, --help            show this help message and exit
  -l SAMPLE_LIST, --sample_list SAMPLE_LIST
                        List of samples to analyze, as a two column tsv file
                        with columns sample and assembly. Can be generated
                        with provided script create_sampleSheet,sh
  -d WORKING_DIRECTORY, --working_directory WORKING_DIRECTORY
                        Working directory where results are saved
  -c CONFIG, --config CONFIG
                        Path to pre-defined config file (Other parameters will
                        be ignored)
  -s SNAKEFILE, --snakefile SNAKEFILE
                        Path to Snakefile of long read assembly pipeline,
                        default is path to Snakefile in same directory
  --no-trimming         Do not trim long reads
  --no-filtering        Do not perform quality and length filtering on long
                        reads
  --no-unicycler        Do not Perform unicycler assembly
  --no-flye             Do not Perform flye assembly
  --no-pilon            Do not Perform pilon polising on flye assembly
  --no-short            Do not Perform mapping with short reads
  --no-long             Do only perorm unicycler assembly, with short reads
                        only
  --pacbio              Long reads are pacbio reads. Do not run Nanostat
  --trimming_minscore TRIMMING_MINSCORE
                        Minimum score to keep reads in trimming, default=60
  --filtering_minquality FILTERING_MINQUALITY
                        Minimum quality score of reads to keep reads in
                        filtering, default=7
  --filtering_minlength FILTERING_MINLENGTH
                        Minimum length to keep reads in trimming, default=1000
  --filtering_extra FILTERING_EXTRA
                        Extra parameters for filtering, default=""
  --unicycler_mode UNICYCLER_MODE
                        unicycler mode option: Choose from
                        conservative,normal,bold, default="normal"
  --unicycler_keep UNICYCLER_KEEP
                        unicycler keep option: choose 0 or 1, see unicycler
                        --help for more info, default=0
  --unicycler_extra UNICYCLER_EXTRA
                        Extra parameters for unicycler, enclose in ',
                        default=""
  --flye_read_type FLYE_READ_TYPE
                        flye read type: choose from '--pacbio-raw' '--pacbio-
                        corr' '--pacbio-hifi' '--nano-raw' '--nano-corr' '--
                        subassemblies', default="--nano-raw"
  --flye_iterations FLYE_ITERATIONS
                        Number of self-polising iterations in flye, default=3
  --flye_extra FLYE_EXTRA
                        Extra parameters for flye, enclose in ', default=""
  --pilon_fix PILON_FIX
                        Type of errors to fix with pilon: choose from
                        snps,indels,gaps,local,all,bases,none, default="all"
  --pilon_iterations PILON_ITERATIONS
                        Number of polishing iterations in pilon, default=5
  --pilon_extra PILON_EXTRA
                        Extra parameters for pilon, enclose in ', default=""
  --abricate_databases ABRICATE_DATABASES
                        List of databases to search for genes,
                        default=["ncbi","plasmidfinder"]
  --abricate_minid ABRICATE_MINID
                        Minimum identity in abricate, default=80
  --abricate_mincov ABRICATE_MINCOV
                        Minimum coverage in abricate, default=80
  --abricate_mincov_contigends ABRICATE_MINCOV_CONTIGENDS
                        Minimum coverage in abricate, default=20
  --abricate_db ABRICATE_DB
                        Path to abricate and prokka databases, default is
                        database of conda environment
  --platon_db PLATON_DB
                        Path to location of platon database for plasmid
                        prediction: plasmid_db; default: /path/2/milonga/databases/platon
  --kraken2db KRAKEN2DB
                        Path to kraken2 database; default: /path/2/milonga/databases/kraken2
  --taxonkit_db TAXONKIT_DB
                        Path to taxonkit_db; default: /path/2/milonga/databases/taxonkit
  --taxlevel TAXLEVEL   taxonomic level for kraken2 contig parsing,
                        default="species"
  -t THREADS, --threads THREADS
                        Number of Threads to use. Note that samples can only
                        be processed sequentially due to the required database
                        access. However the allele calling can be executed in
                        parallel for the different loci, default = 10
  -n, --dryrun          Snakemake dryrun. Only calculate graph without
                        executing anything
  --threads_sample THREADS_SAMPLE
                        Number of Threads to use per sample, default = 10
  --forceall            Snakemake force. Force recalculation of all steps
  --unlock              unlock snakemake
  --logdir LOGDIR       Path to directory whete .snakemake output is to be
                        saved
```


### Using snakemake

Example config file is available in the repo: config_example.yaml


```
snakemake --snakefile path/2/repo/pipeline.smk --configfile path/to/config.yaml --cores 10
```

## Output


### Folders per sample

{sample}/assembly_alignment
{sample}/filtered
{sample}/flye
{sample}/reports
{sample}/stats
{sample}/trimmed
{sample}/unicycler

{sample}/reports/samplereport_C113.html
{sample}/reports/stats_flye.tsv
{sample}/reports/stats_unicycler.tsv
{sample}/reports/summary_assembly.tsv
{sample}/reports/summary_qc.tsv



### Reports
reports/software_versions.tsv
reports/summary_assembly.tsv
reports/summary_qc.tsv
reports/summary_reads.tsv
reports/summary_report.html


## Test data


### Activate conda environment

```
conda activate milonga
```

### Navigate to repo

```
cd path/2/repo # CHANGE
```


### Download test data

```
bash scripts/milonga_setup.sh --status --test_data
```

### Test command

```
python milonga.py -d test_data/milonga -l test_data/samples.tsv -n
```

### Execution

```
python milonga.py -d test_data/milonga -l test_data/samples.tsv --threads 10
```




## References

If you use data generated from this pipeline, please cite Deneke et. al, MiLongA: A snakemake workflow for Long-read assembly, 2020, https://gitlab.com/bfr_bioinformatics/milonga

Furthermore, please also reference the tools used in this pipeline

* unicycler
* flye
* abricate
* plasmidfinder database
* amrfinder database
* platon
* snakemake
* pilon
* checkM
* kraken2
* taxonkit
* pafCoordsDotPlotly: https://github.com/tpoorten/dotPlotly/blob/master/pafCoordsDotPlotly.R
