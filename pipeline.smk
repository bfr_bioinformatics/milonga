# Snakemake pipeline for long read and hybrid assembly

import os
import pandas as pd
#import json
#import csv
# import shutil #needed to check if programs exists
# from snakemake.utils import R
shell.executable("bash")


# definitions ---------------------------------------------------------------

# Set snakemake main workdir variable
workdir: config["workdir"]
working_dir =  config["workdir"] # for dotplot

#workdir: "/home/DenekeC/Snakefiles/long_read_assembly_pipeline/testdata/pipeline_results"
#sample_file = "/home/DenekeC/Snakefiles/long_read_assembly_pipeline/testdata/samples.tsv"

#ASSEMBLER = ["unicycler","flye"]

ASSEMBLER = config["assembler"]
#print(ASSEMBLER)

ABRICATE_DBs = config["params"]["abricate"]["applied_databases"]
#print(ABRICATE_DBs)


# samples
#samples = pd.read_table(sample_file, index_col="sample")
samples = pd.read_table(config["samples"], index_col="sample")
samples.index = samples.index.astype('str', copy=False) # in case samples are integers, need to convert them to str



# check for software ---------------

#print("Checking if software treebender is available:",config["paths"]["treebender"])
#check_treebender = shutil.which(config["paths"]["treebender"])
#if not check_treebender:
#    sys.exit('Error: could not find treebinder')
#else:
#    print("treebinder: OK")



# functions -----------------------------------------

def _get_fastq(wildcards,read_pair='fq1'):
    return samples.loc[(wildcards.sample), [read_pair]].dropna()[0]




# rules -----------------------------------------

rule all:
    input:
        "reports/software_versions.tsv",
        #expand("{sample}/stats/nanostats_raw.tsv", sample=samples.index) if config[["params"]][["longreads"]], #rule contains exception handling for PACBIO data
        expand("{sample}/stats/nanostats_raw.tsv", sample=samples.index) if config["params"]["technology"] != "Illumina" else [],
        expand("{sample}/stats/nanostats_filtered.tsv", sample=samples.index) if config["params"]["technology"] != "Illumina" else [],
        expand("{sample}/{assembler}/assembly.fasta", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/{sample}.fasta", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/{sample}.stats.tsv", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/platon/{sample}.tsv", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/abricate/contigendgenes_ncbi.tsv", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/abricate/report_{abricate_database}.tsv", sample=samples.index, assembler = ASSEMBLER, abricate_database = ABRICATE_DBs),
        expand("{sample}/flye/pilon/assembly.pilon.fasta", sample=samples.index) if config["params"]["pilon"]["do_pilon"] else [],
        expand("{sample}/{assembler}/stats/coverage_{readtype}.bed", sample=samples.index, assembler = ASSEMBLER, readtype = config["params"]["readtype"]),
        expand("{sample}/reports/samplereport_{sample}.html", sample=samples.index),
        "reports/summary_report.html",
# extra
#kraken2
        expand("{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit", sample=samples.index, assembler = ASSEMBLER) if config["params"]["kraken2"]["do_kraken"] else [],
        expand("{sample}/{assembler}/kraken2/{sample}.kraken.fromcontigs.summary.tsv", sample=samples.index, assembler = ASSEMBLER) if config["params"]["kraken2"]["do_kraken"] else [],
#        #checkm
        expand("{sample}/{assembler}/checkm/results_checkM_genome.tsv", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/checkm/results_checkM_tree_qa_genome.tsv", sample=samples.index, assembler = ASSEMBLER),
# selfalignment
        expand("{sample}/{assembler}/selfalignment/{sample}.paf", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/selfalignment/{sample}.summary.tsv", sample=samples.index, assembler = ASSEMBLER),
        # cross alignment
        expand("{sample}/assembly_alignment/{sample}_flye_vs_unicycler.paf", sample=samples.index) if config["params"]["flye"]["do_flye"] & config["params"]["unicycler"]["do_unicycler"] else [],


rule all_extra:
    input:
#kraken2
        expand("{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit", sample=samples.index, assembler = ASSEMBLER) if config["params"]["kraken2"]["do_kraken"] else [],
        expand("{sample}/{assembler}/kraken2/{sample}.kraken.fromcontigs.summary.tsv", sample=samples.index, assembler = ASSEMBLER) if config["params"]["kraken2"]["do_kraken"] else [],
#        #checkm
        expand("{sample}/{assembler}/checkm/results_checkM_genome.tsv", sample=samples.index, assembler = ASSEMBLER),
        expand("{sample}/{assembler}/checkm/results_checkM_tree_qa_genome.tsv", sample=samples.index, assembler = ASSEMBLER),
# checkm taxonomy
#        expand("{sample}/{assembler}/checkm_taxonomy/results_checkM_genome.tsv", sample=samples.index, assembler = ASSEMBLER), # do not do both! but working
# selfalignment
        expand("{sample}/{assembler}/selfalignment/{sample}.paf", sample=samples.index, assembler = ASSEMBLER),
        #expand("{sample}/{assembler}/selfalignment/{sample}.html", sample=samples.index, assembler = ASSEMBLER), # not working
        expand("{sample}/{assembler}/selfalignment/{sample}.summary.tsv", sample=samples.index, assembler = ASSEMBLER),
        # make reports
        expand("{sample}/reports/samplereport_{sample}.html", sample=samples.index),
        "reports/summary_report.html"



rule all_unicycler:
    input:
        expand("{sample}/unicycler/assembly.fasta", sample=samples.index),

rule all_flye:
    input:
        expand("{sample}/flye/assembly.fasta", sample=samples.index),

rule all_flye_polishing:
    input:
        expand("{sample}/flye/pilon/assembly.fasta", sample=samples.index),


rule all_reports:
    input:
        expand("{sample}/reports/samplereport_{sample}.html", sample=samples.index),
        "reports/summary_report.html"


# include other rules
include: "rules/pilon_recursive.smk" # pilon polishing of flye
# load extra rules
include: "rules/extra_rules.smk"




# sketch rules ------------------------------------------------------------------------


# sample sheet
# old
#sample nanopore_run barcode_number illumina_instrument illumina_run short1 short2 illumina_sequencing_date Mbp Target_coverage
# new
#sample long_reads short1 short2


#echo -e "sample\tlong_reads\tshort1\tshort2" > samples.tsv
#echo -e "sample\tlong_reads\tshort1\tshort2" >> samples.tsv

# alternative
#sample nanopore_run barcode_number short1 short2


# create_samplesheet
# merge fastq files
# renames files
# cat $readDir/*.fastq | gzip > $long_raw


# parameters:
# ONT/pacbio; raw/pre-assembled
# --unicycler (=unicycler)
# --flye --polish
# --long-only
# --classify

# --minimum_length_threshold
# --subsampling
# --qc_filter

# unicycler options --bold, --conservative
# flye option
# --outdir



#rule Compute_MinonQC:
#    input:
#        sequencing_summary.txt
#    shell:
#        "/usr/bin/Rscript /cephfs/abteilung4/NGS/software/minion_qc/MinIONQC.R -i $projectDir/sequencing_summary.txt -o $projectDir/MinIONQC"
        
        
#rule run_NanoStat:
#    input:
#        fastq, trimmed fastq
#        sequencing_summary.txt



# rule qc_summary_per_sample
# + N50





# rule adapter_trimming
rule rule adapter_trimming:
    input:
        longreads = lambda wildcards: _get_fastq(wildcards, 'long'),
    output:
        trimmed_long = "{sample}/trimmed/{sample}.fastq.gz"
    #message: "Running qcat on {wildcards.sample}"
    message: "Running porechop on {wildcards.sample}"
    #conda:
    #   "envs/fastp.yaml"
    log:
       #"logs/qcat_{sample}.log"
       "logs/porechop_{sample}.log"
    threads:
        config["params"]["threads"]
    params:
        minscore = config["params"]["trimming"]["minscore"], #60, # Minimum barcode score. Barcode calls with a lower score will be discarded. Must be between 0 and 100.
        temp_fastq = "{sample}/trimmed/{sample}.tmp.fastq",
        barcodedir = "{sample}/trimmed/qcat"
    shell:
#        """
#        qcat --fastq {input.longreads} --output {output.trimmed_long} --trim --tsv --threads {threads} --detect-middle --min-score {params.minscore} &> {log}
#        """
# qcat
        #"""
        #gunzip -k {input.longreads} > {param.temp_fastq}
        #mkdir -p {params.barcodedir}
        #qcat --fastq {param.temp_fastq} --barcode_dir {params.barcodedir} --trim --tsv --threads {threads} --detect-middle --min-score {params.minscore} &> {log}
        #gzip {params.barcodedir}/none.fastq > {output.trimmed_long}
        #rm {param.temp_fastq}
        #"""
# porechop
        """
        porechop -i {input.longreads} -o {output.trimmed_long} --threads {threads} &> {log}
        """
        # TODO add more options to porechop if desired

# input: long_reads
# tool: qcat
#  -f FASTQ, --fastq FASTQ
#                        Barcoded read file
#  -b BARCODE_DIR, --barcode_dir BARCODE_DIR
#                        If specified, qcat will demultiplex reads to this
#                        folder
#  -o OUTPUT, --output OUTPUT
#                        Output file trimmed reads will be written to (default:
#                        stdout).
#  --min-score MIN_QUAL  Minimum barcode score. Barcode calls with a lower
#                        score will be discarded. Must be between 0 and 100.
#                        (default: 60)
#  --detect-middle       Search for adapters in the whole read
#  -t THREADS, --threads THREADS
#                        Number of threads. Only works with in guppy mode
#  --tsv                 Prints a tsv file containing barcode information each
#                        read to stdout.
#  --trim                Remove adapter and barcode sequences from reads.




# rule read_filtering
# filtlong
#filtlong -1 $short1 -2 $short2 --min_length 1000 --keep_percent 90 --target_bases $target_bases --trim --split 500 $long_raw
# replace by nanofilt?
# options
# length filter
# coverage filter
# quality filter
rule read_filtering:
    input:
        trimmed_long = "{sample}/trimmed/{sample}.fastq.gz"
    output:
        filtered_long = "{sample}/filtered/{sample}.fastq.gz"
    message: "Running nanofilt on {wildcards.sample}"
    #conda:
    #   "envs/fastp.yaml"
    log:
       "logs/nanofilt_{sample}.log"
    threads:
        config["params"]["threads"]
    params:
        minquality = config["params"]["filtering"]["minquality"],#7, 
        minlength = config["params"]["filtering"]["minlength"],#1000,
        extra = config["params"]["filtering"]["extra"],#"" #--maxlength --minGC --maxGC --headcrop --tailcrop --readtype 1D
    shell:
        """
        gunzip -c {input} | NanoFilt --quality {params.minquality} --logfile {log} --length  {params.minlength} {params.extra} | gzip > {output}
        """

# TODO make optional ?

#NanoFilt [-h] [-v] [--logfile LOGFILE] [-l LENGTH]
                #[--maxlength MAXLENGTH] [-q QUALITY] [--minGC MINGC]
                #[--maxGC MAXGC] [--headcrop HEADCROP] [--tailcrop TAILCROP]
                #[-s SUMMARY] [--readtype {1D,2D,1D2}]
                #[input]

#Perform quality and/or length and/or GC filtering of (long read) fastq data. Reads on stdin.

#General options:
  #-h, --help            show the help and exit
  #-v, --version         Print version and exit.
  #--logfile LOGFILE     Specify the path and filename for the log file.
  #input                 input, uncompressed fastq file (optional)

#Options for filtering reads on.:
  #-l, --length LENGTH   Filter on a minimum read length
  #--maxlength MAXLENGTH Filter on a maximum read length
  #-q, --quality QUALITY Filter on a minimum average read quality score
  #--minGC MINGC         Sequences must have GC content >= to this. Float between 0.0 and 1.0. Ignored if
                        #using summary file.
  #--maxGC MAXGC         Sequences must have GC content <= to this. Float between 0.0 and 1.0. Ignored if
                        #using summary file.

#Options for trimming reads.:
  #--headcrop HEADCROP   Trim n nucleotides from start of read
  #--tailcrop TAILCROP   Trim n nucleotides from end of read

#Input options.:
  #-s, --summary SUMMARY Use albacore or guppy summary file for quality scores
  #--readtype            Which read type to extract information about from summary. Options are 1D, 2D or 1D2

#EXAMPLES

#gunzip -c reads.fastq.gz | NanoFilt -q 10 -l 500 --headcrop 50 | minimap2 genome.fa - | samtools sort -O BAM -@24 -o alignment.bam -
#gunzip -c reads.fastq.gz | NanoFilt -q 12 --headcrop 75 | gzip > trimmed-reads.fastq.gz
#gunzip -c reads.fastq.gz | NanoFilt -q 10 | gzip > highQuality-reads.fastq.gz




#ruleorder: run_unicycler_shortonly > run_unicycler if config["params"]["technology"] == "Illumina" else run_unicycler > run_unicycler_shortonly

#ruleorder: run_unicycler_shortonly > run_unicycler  #WORKS BUT HARD CODED

if config["params"]["technology"] == "Illumina":
    ruleorder: run_unicycler_shortonly > run_unicycler
else:
    ruleorder: run_unicycler > run_unicycler_shortonly
    

rule run_unicycler_shortonly:
    input:
        short1 = lambda wildcards: _get_fastq(wildcards, 'short1'),
        short2 = lambda wildcards: _get_fastq(wildcards, 'short2')
    output:
        unicycler_assembly = "{sample}/unicycler/assembly.fasta"
    message: "Running unicycler on {wildcards.sample} without long reads"
    #conda:
    #   "envs/fastp.yaml"
    log:
       "logs/unicyclershort_{sample}.log"
    threads:
        config["params"]["threads"]
    params:
        extra = config["params"]["unicycler"]["extra"], # other params, e.g. --min_fasta_length, --linear_seqs --vcf
        keep = config["params"]["unicycler"]["keep"], #"1",
        mode = config["params"]["unicycler"]["mode"], #"normal", #{conservative,normal,bold}"
        outdir = "{sample}/unicycler"
    shell:
        """
            echo "unicycler --short1 {input.short1} --short2 {input.short2} --out {params.outdir} --keep {params.keep} --threads {threads} --mode {params.mode} {params.extra}" > {log}
            unicycler --version >> {log}
            unicycler --short1 {input.short1} --short2 {input.short2} --out {params.outdir} --keep {params.keep} --threads {threads} --mode {params.mode} {params.extra} &>> {log}
        """

rule run_unicycler:
    input:
        #do_filter = True
        #long_reads = "{sample}/filtered/{sample}.fastq.gz" if config["params"]["filtering"]["do_filtering"] else "{sample}/trimmed/{sample}.fastq.gz",
        long_reads = "{sample}/filtered/{sample}.fastq.gz" if config["params"]["filtering"]["do_filtering"] else "{sample}/trimmed/{sample}.fastq.gz" if config["params"]["trimming"]["do_trimming"] else lambda wildcards: _get_fastq(wildcards, 'long'),
        #long_reads = "{sample}/filtered/{sample}.fastq.gz",
        short1 = lambda wildcards: _get_fastq(wildcards, 'short1'),
        short2 = lambda wildcards: _get_fastq(wildcards, 'short2')
    output:
        unicycler_assembly = "{sample}/unicycler/assembly.fasta"
    message: "Running unicycler on {wildcards.sample}"
    #conda:
    #   "envs/fastp.yaml"
    log:
       "logs/unicycler_{sample}.log"
    threads:
        config["params"]["threads"]
    params:
        extra = config["params"]["unicycler"]["extra"], # other params, e.g. --min_fasta_length, --linear_seqs --vcf
        keep = config["params"]["unicycler"]["keep"], #"1",
        mode = config["params"]["unicycler"]["mode"], #"normal", #{conservative,normal,bold}"
        outdir = "{sample}/unicycler"
    shell:
        """
            echo "unicycler --short1 {input.short1} --short2 {input.short2} --long {input.long_reads} --out {params.outdir} --keep {params.keep} --threads {threads} --mode {params.mode} {params.extra}" > {log}
            unicycler --version >> {log}
            unicycler --short1 {input.short1} --short2 {input.short2} --long {input.long_reads} --out {params.outdir} --keep {params.keep} --threads {threads} --mode {params.mode} {params.extra} &>> {log}
        """



#rule parse_unicycler_stats
# circular contigs
# contig length
# number of polished sites ?
rule unicycler_summary:
    input:
        final_assembly = "{sample}/unicycler/{sample}.fasta"
    output:
        unicycler_summary = "{sample}/unicycler/{sample}.stats.tsv"
    message: "Unicycler stats for sample {wildcards.sample}"
    shell:
        """
        echo -e "contig\tlength\tdepth\tcircularity" > {output}
        grep '>' {input} | tr -d '>' | sed 's/length=//' | sed 's/depth=//' | sed 's/circular=//' | sed 's/true/circular/' | sed 's/false/linear/' | tr ' ' $'\t' >> {output}
        """



#rule coverage per contig, short

#TODOrule coverage per contig, long

rule copy_final_unicycler_assembly:
    input:
        unicycler_assembly = "{sample}/unicycler/assembly.fasta"
    output:
        final_assembly = "{sample}/unicycler/{sample}.fasta"
    message: "copy final unicycler for sample {wildcards.sample}"
    shell:
        "cp {input} {output}"



# ---------------------

rule run_flye:
    input:
        #do_filter = True
        long_reads = "{sample}/filtered/{sample}.fastq.gz" if config["params"]["filtering"]["do_filtering"] else "{sample}/trimmed/{sample}.fastq.gz",
        #long_reads = "{sample}/filtered/{sample}.fastq.gz",
    output:
        flye_assembly = "{sample}/flye/assembly.fasta",
        stats = "{sample}/flye/assembly_info.txt",
    message: "Running flye on {wildcards.sample}"
    #conda:
    #   "envs/fastp.yaml"
    log:
       "logs/flye_{sample}.log"
    threads:
        config["params"]["threads"]
    params:
        outdir = "{sample}/flye",
        read_type = config["params"]["flye"]["read_type"], #choose from --pacbio-raw   --pacbio-corr   --pacbio-hifi   --nano-raw   --nano-corr   --subassemblies
        iterations = config["params"]["flye"]["iterations"], #1 number of polishing iterations 
        extra = config["params"]["flye"]["extra"], #--plasmids" # extra options
    shell:
        """
            echo "flye {params.read_type} {input.long_reads} {params.extra} --threads {threads} --iterations {params.iterations} --out-dir {params.outdir} " > {log}
            flye --version >> {log}
            flye {params.read_type} {input.long_reads} {params.extra} --threads {threads} --iterations {params.iterations} --out-dir {params.outdir} &>> {log}
        """

# TODO adapt
# if else
#if [[ {wildcards.genome_size} != "NA" ]]; then
# flye with automatic genome size detection
#else
# flye with pre-defined genome size detection and subsampling for initial assembly
#fi



#--nano-raw {input.long_reads}

# -------------

#usage: flye (--pacbio-raw | --pacbio-corr | --pacbio-hifi | --nano-raw |
             #--nano-corr | --subassemblies) file1 [file_2 ...]
             #--out-dir PATH                                

             #[--genome-size SIZE] [--threads int] [--iterations int]
             #[--meta] [--plasmids] [--trestle] [--polish-target]
             #[--keep-haplotypes] [--debug] [--version] [--help]            
             #[--resume] [--resume-from] [--stop-after]
             #[--hifi-error] [--min-overlap SIZE]
                                                    
#Assembly of long reads with repeat graphs
                                                                                                                            
#optional arguments:
  #-h, --help            show this help message and exit
  #--pacbio-raw path [path ...]
                        #PacBio raw reads
  #--pacbio-corr path [path ...]
                        #PacBio corrected reads
  #--pacbio-hifi path [path ...]
                        #PacBio HiFi reads
  #--nano-raw path [path ...]
                        #ONT raw reads
  #--nano-corr path [path ...]
                        #ONT corrected reads
  #--subassemblies path [path ...]
                        #high-quality contigs input
  #-g size, --genome-size size
                        #estimated genome size (for example, 5m or 2.6g)
  #-o path, --out-dir path
                        #Output directory
  #-t int, --threads int
                        #number of parallel threads [1]
  #-i int, --iterations int
                        #number of polishing iterations [1]
  #-m int, --min-overlap int
                        #minimum overlap between reads [auto]
  #--asm-coverage int    reduced coverage for initial disjointig assembly [not set]
  #--hifi-error float    expected HiFi reads error rate (e.g. 0.01 or 0.001) [0.01]
  #--plasmids            rescue short unassembled plasmids
  #--meta                metagenome / uneven coverage mode
  #--keep-haplotypes     do not collapse alternative haplotypes
  #--trestle             enable Trestle [disabled]
  #--polish-target path  run polisher on the target sequence
  #--resume              resume from the last completed stage
  #--resume-from stage_name
                        #resume from a custom stage
  #--stop-after stage_name
                        #stop after the specified stage completed
  #--debug               enable debug output
  #-v, --version         show program's version number and exit




# -------------



    

# fix headers: remove pilon, add circular information:
rule flye_contig_stats:
    input: 
        flye_assembly = "{sample}/flye/pilon/assembly.pilon.fasta" if config["params"]["pilon"]["do_pilon"] else "{sample}/flye/assembly.fasta",
        stats = "{sample}/flye/assembly_info.txt",
    output:
        flye_summary = "{sample}/flye/{sample}.stats.tsv",
        final_assembly = "{sample}/flye/{sample}.fasta"
    message: "Copy final flye assembly and create contig stats file"
    script:
        "scripts/create_flye_contig_stats.R"




# rules for both final assemblies ---------------------------------------------------------------


#rule classify contigs


#rule classify contigs


rule classify_platon:
    input:
        final_assembly = "{sample}/{assembler}/{sample}.fasta"
    output:
        platin_report = "{sample}/{assembler}/platon/{sample}.tsv"
    message: "Running platon on {wildcards.sample} for {wildcards.assembler}"
    #conda:
    #   "envs/fastp.yaml"
    log:
       "logs/platon_{assembler}_{sample}.log"
    threads:
        config["params"]["threads"]
    params:
        platondb = config["params"]["platon"]["db"],#"/home/DenekeC/Snakefiles/bakcharak/databases/platon",
        outdir = "{sample}/{assembler}/platon",
        sample = "{sample}"
    shell:
        """
        echo "platon --characterize --db {params.platondb} --output {params.outdir} --prefix {params.sample} --threads {threads} --verbose {input}" > {log}
        platon --version >> {log}
        platon --characterize --db {params.platondb} --output {params.outdir} --prefix {params.sample} --threads {threads} --verbose {input} &>> {log}
        """




# run abricate -------------



rule abricate_all:
    input:
        final_assembly = "{sample}/{assembler}/{sample}.fasta"
    output:
        abricate_report = "{sample}/{assembler}/abricate/report_{abricate_database}.tsv"
    message: "Running abricate on {wildcards.assembler} on database {wildcards.abricate_database} for sample {wildcards.sample}"
    log:
       "logs/abricate_{assembler}_{abricate_database}_{sample}.log"
    threads:
        config["params"]["threads"]
    params:
        db = "{abricate_database}",
        minid = config["params"]["abricate"]["minid"],
        mincov = config["params"]["abricate"]["mincov"],
    shell:
        """
        echo "abricate --db {params.db} --nopath --mincov {params.mincov} --minid {params.minid} {input} > {output}" > {log}
        abricate --version >> {log}
        abricate --db {params.db} --nopath --mincov {params.mincov} --minid {params.minid} {input} > {output}
        """






rule abricate_amr_contig_ends:
    input:
        final_assembly = "{sample}/{assembler}/{sample}.fasta"
    output:
        abricate_report = "{sample}/{assembler}/abricate/report-mincov20_ncbi.tsv"
    message: "Running abricate on {wildcards.sample} for {wildcards.assembler} contig_ends"
    log:
       "logs/abricate_{assembler}_ncbi_{sample}_contig_ends.log"
    threads:
        10, #config["params"]["threads"]
    params:
        db = "ncbi",
        minid = config["params"]["abricate"]["minid"],
        mincov = config["params"]["abricate"]["mincov_contigends"],
    shell:
        """
        echo "abricate --db {params.db} --nopath --mincov {params.mincov} --minid {params.minid} {input} > {output}" > {log}
        abricate --version >> {log}
        abricate --db {params.db} --nopath --mincov {params.mincov} --minid {params.minid} {input} > {output}
        """

rule abricate_amr_contig_ends_summary:
    input:
        abricate_report = "{sample}/{assembler}/abricate/report-mincov20_ncbi.tsv",
        final_assembly = "{sample}/{assembler}/{sample}.fasta"
    output:
        summary = "{sample}/{assembler}/abricate/contigendgenes_ncbi.tsv"
    message: "Summary abricate on {wildcards.sample} for {wildcards.assembler} contig_ends"
    log:
       "logs/summary_abricate_{assembler}_ncbi_{sample}_contig_ends.log"
    script:
        "scripts/find_contig_end_genes.R"







# TODO duplication warning: if plasmidfinder or abricate show duplicate gene on same contig, give warning
# use summary function?



# TODO bedfile of short read and long read mapping
# use minimap?


rule map_short_reads:
    input:
        final_assembly = "{sample}/{assembler}/{sample}.fasta",
        short1 = lambda wildcards: _get_fastq(wildcards, 'short1'),
        short2 = lambda wildcards: _get_fastq(wildcards, 'short2')
    output:
        mapping_short = "{sample}/{assembler}/stats/mapping_shortreads.bam", # TODO make temporary
    message: "Mapping short reads against {wildcards.assembler} for sample {wildcards.sample}"
    threads: config["params"]["threads"]
    log: "logs/minimap_shortreads_{assembler}_{sample}.log"
    params:
        extra = ""
    shell:
        """
        echo "minimap2 -t {threads} -x sr -a {input.final_assembly} {input.short1} {input.short2} | samtools sort - -o {output} --threads {threads}" | tee {log}
        echo "minimap2 --version" >> {log}
        minimap2 {params.extra} -t {threads} -x sr -a {input.final_assembly} {input.short1} {input.short2} | samtools sort - -o {output} --threads {threads} &>> {log}
        """


rule map_long_reads:
    input:
        final_assembly = "{sample}/{assembler}/{sample}.fasta",
        long_reads = "{sample}/filtered/{sample}.fastq.gz" if config["params"]["filtering"]["do_filtering"] else "{sample}/trimmed/{sample}.fastq.gz",
    output:
        mapping_long = "{sample}/{assembler}/stats/mapping_longreads.bam", # TODO make temporary
    message: "Mapping long reads against {wildcards.assembler} for sample {wildcards.sample}"
    threads: config["params"]["threads"]
    log: "logs/minimap_longreads_{assembler}_{sample}.log"
    params:
        extra = "", # -H for pacbio
        mode = "map-ont" # map-pb # TODO contig
    shell:
        """
        echo "minimap2 -t {threads} -x {params.mode} -a {input.final_assembly} {input.long_reads} | samtools sort - -o {output} --threads {threads}" | tee {log}
        echo "minimap2 --version" >> {log}
        minimap2 {params.extra} -t {threads} -x {params.mode} -a {input.final_assembly} {input.long_reads} | samtools sort - -o {output} --threads {threads} &>> {log}
        """


    #samtools view  --threads {threads} -S -b -u - | 
    
    
# -x preset (always applied before other options; see minimap2.1 for details) []
                 # - map-pb/map-ont: PacBio/Nanopore vs reference mapping
                 # - ava-pb/ava-ont: PacBio/Nanopore read overlap
                 # - asm5/asm10/asm20: asm-to-ref mapping, for ~0.1/1/5% sequence divergence
                 # - splice: long-read spliced alignment
                 # - sr: genomic short-read mapping
# -H           use homopolymer-compressed k-mer (preferrable for PacBio)
    # -a           output in the SAM format (PAF by default)
# minimap2 [options] <target.fa>|<target.idx> [query.fa] [...]




#rule map_long_reads:
    #input:
        #final_assembly = "{sample}/{assembler}/{sample}.fasta",
        #long_reads = "{sample}/filtered/{sample}.fastq.gz" if config["params"]["filtering"]["do_filtering"] else "{sample}/trimmed/{sample}.fastq.gz",
    #output:
    #message:
    #threads:
    #log:
    #scripts:

#rule get_coverage_map:
    #input:
    #output:
    #message:
    #threads:
    #log:
    #scripts:

rule run_bedtools:
    input:
        #bam = "results/{sample}/{refdir}/{reffile}.bam"
        #bam = "{sample}/{assembler}/stats/{sample}_longreads.bam",
        bam = "{sample}/{assembler}/stats/mapping_{readtype}.bam",
    output:
        #bed = "results/{sample}/{refdir}/{reffile}.bed"
        bed = "{sample}/{assembler}/stats/coverage_{readtype}.bed",
    message: "Running bedtools on {wildcards.sample} and assembler {wildcards.assembler} for {wildcards.readtype}"
    log: "logs/bedtools_{sample}_{assembler}_{readtype}.log"
    threads: config["params"]["threads"]
    shell:
        """
        echo "samtools view -b {input.bam} | genomeCoverageBed -ibam stdin -d > {output.bed} 2> {log}" | tee {log}
        samtools view -b {input.bam} | genomeCoverageBed -ibam stdin -d > {output.bed} 2> {log}
        """

# TODO need to compress or delete bed file?



rule software_versions:
    output:
        versions="reports/software_versions.tsv"
    message:
        "Parsing software versions"
    log:
        "logs/software_versions.log"
    shell:
        """
        echo -e "Software Version" > {output.versions}
        porechop_version=`porechop --version`
        echo "porechop $porechop_version" >> {output.versions}
        NanoFilt --version >> {output.versions}
        unicycler --version >> {output.versions}
        flye_version=`flye --version`
        echo "flye $flye_version" >> {output.versions}
        #flye --version >> {output.versions}
        abricate --version >> {output.versions}
        platon --version >> {output.versions}
        NanoStat --version >> {output.versions}
        # extra
        kraken2 --version | sed 's/version //' | head -n 1 >> {output.versions}
        checkm | grep -oE 'CheckM v[0-9.]+' >> {output.versions}
        minimap2_version=`minimap2 --version`
        echo "minimap2 $minimap2_version" >> {output.versions}
        """


# 

rule sample_report:
    input:
        software_versions = "reports/software_versions.tsv",
        # read stats
        nanostats_raw = "{sample}/stats/nanostats_raw.tsv",
        nanostats_filtered = "{sample}/stats/nanostats_filtered.tsv",
        # flye
        flye_coverage_short = "{sample}/flye/stats/coverage_shortreads.bed" if "shortreads" in config["params"]["readtype"] and config["params"]["flye"]["do_flye"] else [],
        flye_coverage_long = "{sample}/flye/stats/coverage_longreads.bed" if "longreads" in config["params"]["readtype"] and config["params"]["flye"]["do_flye"] else [],
        flye_fasta = "{sample}/flye/{sample}.fasta" if config["params"]["flye"]["do_flye"] else [],
        flye_contigstats = "{sample}/flye/{sample}.stats.tsv" if config["params"]["flye"]["do_flye"] else [],
        flye_platon = "{sample}/flye/platon/{sample}.tsv" if config["params"]["flye"]["do_flye"] else [],
        flye_abricate_contigends = "{sample}/flye/abricate/contigendgenes_ncbi.tsv" if config["params"]["flye"]["do_flye"] else [],
        flye_abricate_ncbi = "{sample}/flye/abricate/report_ncbi.tsv" if config["params"]["flye"]["do_flye"] else [],
        flye_abricate_plasmidfinder = "{sample}/flye/abricate/report_plasmidfinder.tsv" if config["params"]["flye"]["do_flye"] else [],
        # extra
        flye_krakensummary = "{sample}/flye/kraken2/{sample}.kraken.fromcontigs.summary.tsv"  if config["params"]["flye"]["do_flye"] else [],
        flye_krakencontigs = "{sample}/flye/kraken2/{sample}.fromcontigs.taxonkit" if config["params"]["flye"]["do_flye"] else [],
        flye_checkm_results = "{sample}/flye/checkm/results_checkM_genome.tsv" if config["params"]["flye"]["do_flye"] else [],
        flye_checkm_qa = "{sample}/flye/checkm/results_checkM_tree_qa_genome.tsv" if config["params"]["flye"]["do_flye"] else [],
        flye_selfalignment = "{sample}/flye/selfalignment/{sample}.paf" if config["params"]["flye"]["do_flye"] else [],
        flye_selfalignment_summary = "{sample}/flye/selfalignment/{sample}.summary.tsv" if config["params"]["flye"]["do_flye"] else [],
        # unicycler
        unicycler_coverage_short = "{sample}/unicycler/stats/coverage_shortreads.bed" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_coverage_long = "{sample}/unicycler/stats/coverage_longreads.bed" if "longreads" in config["params"]["readtype"] and config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_fasta = "{sample}/unicycler/{sample}.fasta" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_contigstats = "{sample}/unicycler/{sample}.stats.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_platon = "{sample}/unicycler/platon/{sample}.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_abricate_contigends = "{sample}/unicycler/abricate/contigendgenes_ncbi.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_abricate_ncbi = "{sample}/unicycler/abricate/report_ncbi.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_abricate_plasmidfinder = "{sample}/unicycler/abricate/report_plasmidfinder.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
# extra
        unicycler_krakensummary = "{sample}/unicycler/kraken2/{sample}.kraken.fromcontigs.summary.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_krakencontigs = "{sample}/unicycler/kraken2/{sample}.fromcontigs.taxonkit" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_checkm_results = "{sample}/unicycler/checkm/results_checkM_genome.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_checkm_qa = "{sample}/unicycler/checkm/results_checkM_tree_qa_genome.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_selfalignment = "{sample}/unicycler/selfalignment/{sample}.paf" if config["params"]["unicycler"]["do_unicycler"] else [],
        unicycler_selfalignment_summary = "{sample}/unicycler/selfalignment/{sample}.summary.tsv" if config["params"]["unicycler"]["do_unicycler"] else [],
        # combination
        flye_vs_unicycler_alignment = "{sample}/assembly_alignment/{sample}_flye_vs_unicycler.paf" if config["params"]["flye"]["do_flye"] & config["params"]["unicycler"]["do_unicycler"] else [],
    output:
        "{sample}/reports/samplereport_{sample}.html"
    message: "Creating report for sample {wildcards.sample}"
    params: # xxx
        do_flye = config["params"]["flye"]["do_flye"],
        do_unicycler = config["params"]["unicycler"]["do_unicycler"],
        working_dir = config["workdir"],
        parameters = config["params"],
        readtype = config["params"]["readtype"],
        sample = "{sample}",
        functions_file = {workflow.basedir},
    script:
        "scripts/sample_report.Rmd"


# 

# TODO prokka ?


rule run_summary:
    input:
        software_versions = "reports/software_versions.tsv",
        samplereports = expand("{sample}/reports/samplereport_{sample}.html",sample=samples.index),
        readstats = expand("{sample}/stats/nanostats_raw.tsv",sample=samples.index),
        readstats_filtered = expand("{sample}/stats/nanostats_filtered.tsv",sample=samples.index)
    output: "reports/summary_report.html"
    message: "Creating summary report for entire run"
    #log:
    params:
        do_flye = config["params"]["flye"]["do_flye"],
        do_unicycler = config["params"]["unicycler"]["do_unicycler"],
        working_dir = config["workdir"],
        parameters = config["params"],
        readtype = config["params"]["readtype"],
        technology = config["params"]["technology"],
    script:
        "scripts/summary_report.Rmd"



# TODO stats file
#"{sample}/reports/samplestats_{sample}.tsv"

#rule summary_report:
#rule summary_report:
#input:
    #expand("{sample}/reports/samplereport_{sample}.html",sample=samples.index),
    #expand("{sample}/reports/samplestats_{sample}.tsv",sample=samples.index),
    #expand("{sample}/stats/nanostats_raw.tsv",sample=samples.index),
#output: 
    #"summary_report.html"
#message: "Creating summary report"
#script:
    #"scripts/sumamry_report.Rmd"



# TODO: also do for filtered data?
rule nanostats_raw_reads:
    input:
        longreads = lambda wildcards: _get_fastq(wildcards, 'long'),
    output:
        "{sample}/stats/nanostats_raw.tsv"
    message: "Run NanoStats on raw reads for sample {wildcards.sample}"
    threads: config["params"]["threads"]
    params:
        outdir = "{sample}/stats",
        technology = config["params"]["technology"],
        tmpfile = "{sample}/stats/readstats_length.tmp",
        tmpfile2 = "{sample}/stats/readstats_quality.tmp"
    shell: 
        """
         if [[ {params.technology} == "ONT" ]]; then
            echo "Running NanoStat"
            NanoStat --threads {threads} --fastq  {input} --tsv -o {params.outdir} -n nanostats_raw.tsv
         else
            echo "Running custom Read QC"
             # compute some stuff
             bioawk -c fastx '{{print length($seq)}}' {input.longreads} | sort -nr > {params.tmpfile}
             bioawk -c fastx '{{print meanqual($qual)}}' {input.longreads} > {params.tmpfile2} #TODO create empty file
             
             number_of_reads=`wc -l {params.tmpfile} | cut -f 1 -d ' '`
             number_of_bases=`awk '{{sum=sum+$1}}; END{{print sum}}' {params.tmpfile}`
             mean_read_length=`awk '{{sum=sum+$1}}; END{{print sum/NR}}' {params.tmpfile}`
             mean_qual=`awk '{{sum=sum+$1}}; END{{print sum/NR}}' {params.tmpfile2}`
             longest_read=`head -n 1 {params.tmpfile}`
             
             echo -e "Metrics\tdataset" > {output}
             echo -e "number_of_reads\t$number_of_reads" >> {output}
             echo -e "number_of_bases\t$number_of_bases" >> {output}         
             echo -e "mean_read_length\t$mean_read_length" >> {output}
             echo -e "mean_qual\t$mean_qual" >> {output}
             echo -e "longest_read\t$longest_read" >> {output}
         fi
        """


rule nanostats_filtered_reads:
    input:
        longreads = "{sample}/filtered/{sample}.fastq.gz" if config["params"]["filtering"]["do_filtering"] else "{sample}/trimmed/{sample}.fastq.gz" if config["params"]["trimming"]["do_trimming"] else lambda wildcards: _get_fastq(wildcards, 'long'),
    output:
        "{sample}/stats/nanostats_filtered.tsv"
    message: "Run NanoStats on filtered reads for sample {wildcards.sample}"
    threads: config["params"]["threads"]
    params:
        outdir = "{sample}/stats",
        technology = config["params"]["technology"],
        tmpfile = "{sample}/stats/readstats_length_filtered.tmp",
        tmpfile2 = "{sample}/stats/readstats_quality_filtered.tmp"
    shell: 
        """
         if [[ {params.technology} == "ONT" ]]; then
            echo "Running NanoStat"
            NanoStat --threads {threads} --fastq  {input} --tsv -o {params.outdir} -n nanostats_filtered.tsv
         else
            echo "Running custom Read QC"
             # compute some stuff
             bioawk -c fastx '{{print length($seq)}}' {input.longreads} | sort -nr > {params.tmpfile}
             bioawk -c fastx '{{print meanqual($qual)}}' {input.longreads} > {params.tmpfile2} #TODO create empty file
             
             number_of_reads=`wc -l {params.tmpfile} | cut -f 1 -d ' '`
             number_of_bases=`awk '{{sum=sum+$1}}; END{{print sum}}' {params.tmpfile}`
             mean_read_length=`awk '{{sum=sum+$1}}; END{{print sum/NR}}' {params.tmpfile}`
             mean_qual=`awk '{{sum=sum+$1}}; END{{print sum/NR}}' {params.tmpfile2}`
             longest_read=`head -n 1 {params.tmpfile}`
             
             echo -e "Metrics\tdataset" > {output}
             echo -e "number_of_reads\t$number_of_reads" >> {output}
             echo -e "number_of_bases\t$number_of_bases" >> {output}         
             echo -e "mean_read_length\t$mean_read_length" >> {output}
             echo -e "mean_qual\t$mean_qual" >> {output}
             echo -e "longest_read\t$longest_read" >> {output}
         fi
        """



#"NanoStat --threads {threads} --fastq  {input} --tsv -o {params.outdir} -n nanostats_raw.tsv"

    #"""
        #echo "NanoStat --threads {threads} --fastq  {input} --tsv -o {params.outdir} -n nanostats_raw.tsv"
        #NanoStat --threads {threads} --fastq  {input} --tsv -o {params.outdir} -n nanostats_raw.tsv
    #"""
# TODO other program for pacbio?






# -------------------
# rule prokka


# --------------------
# rule assembly_comparison
# unicycler vs. flye contigs

# -------------------
# reporting


# per run run report
# qc stats
# assembly stats



# per sample report
# summary per method
# method comparison
# steps summary

# resgenes
# plasmids








# status
# logging information---------


onstart:
    shell('echo -e "running\t`date`" > pipeline_status.txt')

onsuccess:
    shell('echo -e "success\t`date`" > pipeline_status.txt')

onerror:
    shell('echo -e "error\t`date`" > pipeline_status.txt'),
    print("An error occurred")













