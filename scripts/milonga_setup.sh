#!/bin/bash

## [Goal] Install mamba, conda env, complement conda env, download databases, download test data for milonga
## [Author] Holger Brendebach, Carlus Deneke

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, # ADD e
trap cleanup SIGINT SIGTERM ERR EXIT

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
repo_path=$(dirname $script_path)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "ERROR: File not found: $script_path/helper_functions.sh" && exit 1
source "$script_path/helper_functions.sh"

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_real [OPTIONS]

${bold}Description:${normal}
This script completes the installation of the MiLongA pipeline. The openssl library is required for hashing the downloaded files.
for MiLongA installations from Gitlab use the option set [--mamba --databases].
For more information, please visit https://gitlab.com/bfr_bioinformatics/milonga

${bold}Options:${normal}
  -m, --mamba                Install the latest version of 'mamba' to the Conda base environment and
                             create the MiLongA environment from the git repository recipe
  -d, --databases            Download databases to <MiLongA>/download and extract them in <MiLongA>/databases
  -t, --test_data            Download test data to <MiLongA>/download and extract them in <MiLongA>/test_data
  -s, --status               Show installation status
  -f, --force                Force overwrite for downloads in <MiLongA>/download
  -k, --keep_downloads       Do not remove downloads after extraction
  -v, --verbose              Print script debug info
  -h, --help                 Show this help

USE
  exit 0
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  [[ -d "$repo_path/download" ]] && [[ -n ${arg_keep_dl+x} ]] && [[ "$arg_keep_dl" == false ]] && rm -R $repo_path/download
}

parse_args() {
  local args=("$@")

  ## default values of variables
  conda_recipe="milonga.yaml"

  ## default values of switches
  conda_status=false
  mamba_status=false
  bioconda_status=false

  arg_mamba=false
  arg_databases=false
  arg_test_data=false
  arg_status=false
  arg_force=false
  arg_keep_dl=false

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      -dev | --dev) dev_notes $script_real ;;
      -v | --verbose)
        set -x
        shift
        ;;
      -m | --mamba)
        arg_mamba=true
        shift
        ;;
      -d | --databases)
        arg_databases=true
        shift
        ;;
      -t | --test_data)
        arg_test_data=true
        shift
        ;;
      -s | --status)
        arg_status=true
        shift
        ;;
      -f | --force)
        arg_force=true
        shift
        ;;
      -k | --keep_downloads)
        arg_keep_dl=true
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. For the manual, type ${blue}bash $script_real --help${normal}"

  ## checks
  [[ $(basename $script_path) != "scripts" ]] && die "This setup script does not reside in its original installation directory. This is a requirement for proper execution. Aborting..."

  return 0
}

## Parse Arguments --------------------------------------------------
parse_args "$@"

## (2) Script logic

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
repo_path:|$repo_path
" | column -t -s "|"
}

## Modules --------------------------------------------------


download_sra() {
 # Init
  download_success=false
  # local file name
  
  
  local_archive="${extraction_directory}/`basename $remote_archive`"
  echo "local_archive: $local_archive"
  # Download
  if ( [[ ! -f $local_archive ]] || [[ $arg_force == true ]] ); then
    logentry "Downloading $remote_archive to $extraction_directory"
    wget --directory-prefix=$extraction_directory $remote_archive && download_success=true
    # Verify Integrity of Download
    [[ "$download_success" == true  ]] && [[ -s $extraction_directory ]] && openssl dgst -r -md5 $local_archive >> $hashfile
    [[ "$download_success" == false ]] && logwarn "A download error occurred."
  else
    logwarn "The file $local_archive already exists. Skipping download."
  fi

}

download_file() {
  # Init
  download_success=false

  # Download
  if ( [[ ! -f $local_archive ]] || [[ $arg_force == true ]] ); then
    logentry "Downloading $remote_archive to $local_archive"
    wget --output-document $local_archive $remote_archive && download_success=true

    # Verify Integrity of Download
    [[ "$download_success" == true  ]] && [[ -s $local_archive ]] && openssl dgst -r -md5 $local_archive >> $hashfile
    [[ "$download_success" == false ]] && logwarn "A download error occurred."
  else
    logwarn "The file $local_archive already exists. Skipping download."
  fi

  if [[ -f $local_archive ]] && ( [[ "$download_success" == true  ]] || [[ $arg_force == true ]] ); then
    # Unpack Downloads
    logentry "Extracting archive $(basename $local_archive) to $extraction_directory"
    [[ -d "$extraction_directory" ]] || mkdir -p $extraction_directory
    tar -xzv -f $local_archive -C $extraction_directory
  fi
}

setup_mamba() {
  # checks
  [[ "$bioconda_status" == false ]] && logentry "Installing the latest version of \"mamba\" to the Conda base environment."
  [[ "$bioconda_status" == true  ]] && die "This is a Bioconda installation. There is no need to setup Mamba or an environment for MiLongA."

  source $conda_base/etc/profile.d/conda.sh
  [[ "$mamba_status" == false ]] && conda install --channel conda-forge mamba
  [[ "$mamba_status" == true ]] && logwarn "Skipping Mamba installation. Mamba is already detected."

  # Create Conda Environment for MiLongA
  logentry "Creating the MiLongA environment with the Conda recipe: $repo_path/envs/${conda_recipe}"
  set +eu  # workaround: see https://github.com/conda/conda/issues/8186
  [[ -z "${conda_env_path}" ]] && { mamba env create -f $repo_path/envs/${conda_recipe} || true; }
  [[ -n "${conda_env_path}" ]] && logwarn "A Conda environment with the name \"$conda_env_name\" is already present. Skipping environment creation."
  # conda activate $conda_env_name
  set -eu

  logentry "${green}Environment installation completed.${normal}"
}

download_databases() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && mkdir -p $repo_path/download

  # Download files

  remote_archive="https://genome-idx.s3.amazonaws.com/kraken/k2_standard_08gb_20230314.tar.gz"
  local_archive="$repo_path/download/minikraken2.tar.gz"
  hashfile="$repo_path/download/reference_db.sha256"
  extraction_directory="$repo_path/databases/kraken2"
  download_file

  remote_archive="https://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz"
  local_archive="$repo_path/download/taxdump.tar.gz"
  hashfile="$repo_path/download/reference_db.sha256"
  extraction_directory="$repo_path/databases/taxonkit/"
  download_file

  # platon
  remote_archive="https://zenodo.org/record/4066768/files/db.tar.gz"  # 1.6G
  local_archive="$repo_path/download/platon-db.tar.gz"
  hashfile="$repo_path/download/reference_db.md5"
  extraction_directory="$repo_path/databases/platon"
  download_file
  [[ -d "$repo_path/databases/platon/db" ]] && mv --verbose --update $repo_path/databases/platon/db/* $repo_path/databases/platon/ && rm -rf $repo_path/databases/platon/db

  # CleanUp Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $repo_path/download/reference_db.md5 ]] && cat $repo_path/download/reference_db.md5 >> $repo_path/databases/reference_db.md5

  logentry "${green}Database download finished.${normal}"
}

download_test_data() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && mkdir -p $repo_path/download
  [[ ! -d "$repo_path/test_data/fastq" ]] && mkdir -p $repo_path/test_data/fastq

  # Download files
  remote_archive="https://ftp.sra.ebi.ac.uk/vol1/fastq/SRR228/028/SRR22859828/SRR22859828_1.fastq.gz"
  extraction_directory="$repo_path/test_data/fastq/"
  hashfile="$extraction_directory/testdata.sha256"
  download_sra
  
  remote_archive="https://ftp.sra.ebi.ac.uk/vol1/fastq/SRR228/028/SRR22859828/SRR22859828_2.fastq.gz"
  download_sra

  remote_archive="https://ftp.sra.ebi.ac.uk/vol1/fastq/SRR228/091/SRR22859991/SRR22859991_1.fastq.gz"
  download_sra

  # create checksums
  sha256sum $extraction_directory/*.fastq.gz > $repo_path/test_data/test_data.sha256

  # Create Sample Sheet for Test Data
  [[ "$download_success" == true ]] && logentry "Creating test data sample sheet $repo_path/test_data/samples.tsv"
  # NOT so simple here [[ "$download_success" == true ]] && bash ${script_path}/create_sampleSheet.sh --mode ncbi --fastxDir $repo_path/test_data/fastq --outDir $repo_path/test_data
  echo -e "sample\tlong\tshort1\tshort2" > $repo_path/test_data/samples.tsv
  paste <(echo "C113") <(realpath $repo_path/test_data/fastq/SRR22859991_1.fastq.gz) <(realpath $repo_path/test_data/fastq/SRR22859828_1.fastq.gz) <(realpath $repo_path/test_data/fastq/SRR22859828_2.fastq.gz) >> $repo_path/test_data/samples.tsv
  echo

  # CleanUp Downloads
  # [[ "$arg_keep_dl" == false ]] && cat $repo_path/download/test_data.sha256 >> $repo_path/test_data/test_data.sha256

  logentry "${green}Test data download finished.${normal}"
}

check_success() {
  [[ -f $repo_path/databases/kraken2/hash.k2d ]] && status_kraken="${green}OK${normal}"
  [[ -f $repo_path/databases/taxonkit/names.dmp ]] && status_taxonkit="${green}OK${normal}"
  [[ -f $repo_path/databases/platon/refseq-plasmids.nhr ]] && status_platon="${green}OK${normal}"
  logheader "Database Status:"
  echo "
status_kraken:|${status_kraken:-"${red}FAIL${normal}"}
status_taxonkit:|${status_taxonkit:-"${red}FAIL${normal}"}
status_platon:|${status_platon:-"${red}FAIL${normal}"}
" | column -t -s "|"
  echo
}

## (3) Main

logentry "[START] $script_real" # from helper_functions.sh
logentry "Arguments: ${*:-"No arguments provided"}"

## Workflow
conda_recipe_env_name=$(head -n1 $repo_path/envs/${conda_recipe} | cut -d' ' -f2)
check_conda $conda_recipe_env_name
[[ "$arg_mamba" == true ]] && setup_mamba
[[ "$arg_databases" == true ]] && download_databases
[[ "$arg_test_data" == true ]] && download_test_data
[[ "$arg_status" == false ]] && check_conda $conda_recipe_env_name  # check changes to conda
show_info
check_success

logentry "[STOP] $script_real"  # from helper_functions.sh
echo "Thank you for installing MiLongA."
