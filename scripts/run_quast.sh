#!/bin/bash
set -e
set -u
set -o pipefail

# quast script for long read postivkontrolle


## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type run_quast.sh --help"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


## search for "--help" flag
if [[ $@ =~ '--help' || $@ =~ '-h' ]]; then
    echo "Help file ----------------------"
    echo "Basic usage: run_quast.sh path2fasta [Options]"
    echo



    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1

fi

# requires aquamis conda
if ! command -v quast &> /dev/null
then
    echo "quast could not be found. Please activate cond environment containing quast, e.g. conda activate aquamis"
    exit 1
fi


# define data --------------------------

fasta=$1
#fasta="/cephfs/abteilung4/Datentransfer/MinIon_Network/20211008_1722_MN23248_AGY494_5ac7191c/Positivkontrolle/flye/assembly.fasta"


path2script=`dirname "$0"`
path2repo=`dirname $path2script`


# check existence
if [[ ! -f $fasta ]];then
    echo "ERROR: fasta file $fasta does NOT exist"
    exit 1
fi


baseoutdir=$(dirname $fasta)
outdir=$baseoutdir/quast
#outdir=$baseoutdir/quast_polished # for polished only
echo $outdir

mkdir -p $outdir

#refdir="/cephfs/abteilung4/deneke/repositories/milonga/databases/quast"
#refdir="/cephfs/abteilung4/Projects_NGS/Akkreditierung_ONT/positivkontrolle/ref"
refdir="$path2repo/databases/quast"
reffasta=$refdir/reference.fasta  
refgff=$refdir/reference.gff


# check
if [[ ! -f $reffasta ]]; then
    echo "File $reffasta does NOT exist"
    exit 1
fi

if [[ ! -f $refgff ]]; then
    echo "File $refgff does NOT exist"
    exit 1
fi


# extract name of ref
grep '>' $reffasta | head -n 1 | tr -d '^>' | cut -f 1 -d ',' > $outdir/reference_info.txt


# run quast
echo "quast --min-identity 80 -r $reffasta -g $refgff --circos -o $outdir $fasta"
quast --min-identity 80 -r $reffasta -g $refgff --circos -o $outdir $fasta

# run rscript
echo "Rscript $path2script/parse_quast.R $outdir"
Rscript $path2script/parse_quast.R $outdir


echo "Done on `date`. Results in $outdir"
