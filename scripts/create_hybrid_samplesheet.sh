#!/bin/bash
set -e
#set -u
#set -o pipefail

# GOAL: Create sample sheet for loeapi long read assembly pipeline

## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type create_hybrid_samplesheet.sh --help"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


## search for "--help" flag
if [[ $@ =~ '--help' || $@ =~ '-h' ]]; then
    echo "Help file ----------------------"
    echo "Basic usage: create_hybrid_samplesheet.sh rundir [subdir] [--flags]"
    echo
    echo "Positional arguments:"
    echo
    echo "rundir: Name of run (folder name in /cephfs/abteilung4/Datentransfer/MinIon_Network/, e.g. /cephfs/abteilung4/Datentransfer/MinIon_Network/MinIon_20201001)"
    echo
    echo "Optional Positional argument:" 
    echo
    echo "subdir: subdirectory of the rundir where the fast5 files are located, e.g. guppy6_sup"
    echo
    echo "Flags:"
    echo "--interactive Ask before execution"
    echo "--force Force also when samples.tsv already exists"
    echo
    echo "This script parses all barcode dirs in rundir/workspace/pass or workspace/fastq_pass"
    echo "It concatenates all fastq in a barcode dir into a single file whenever more than one fastq file is present."
    echo "If only a single file is present, then these are assumed to follow from barcode index errors and are ignored." 
    echo "Main output is the file samples.tsv in rundir. It has the following columns:"
    echo -e "sample\tlong\tshort1\tshort2"
    echo "Use this file and edit samples and short1 short2 prior to using milonga.py"
    echo "Concatenated reads will be written to rundir/raw"


    [[ "$0" = "$BASH_SOURCE" ]] && exit 0 || return 0

fi


#basedir="/cephfs/abteilung4/Datentransfer/MinIon_Network/MinIon_20201001"
#run="MinIon_20201001"


if [[ $@ =~ '--force' ]]; then
    force=true
else
    force=false
fi

#if [[ $@ =~ '--dryrun' ]]; then
    #dryrun=true
#else
    #dryrun=false
#fi

if [[ $@ =~ '--interactive' ]]; then
    interactive=true
else 
    interactive=false
fi

#rundir="/cephfs/abteilung4/Datentransfer/MinIon_Network/MinIon_20201001"


rundir=$1

if [[ ! $rundir ]]; then
    echo "ERROR: $rundir does NOT exist"
    exit 1 
else
    echo "You selected rundir $rundir"
fi


# subdir
#if [ $# -gt 1 ]; then
#if [[ $# -gt 1 && (! $2 =~ ^--) ]]; then
    ##if [[ $2 =~ ^-- ]]; then
    #echo "Is flag:$2"
    #else
    #echo "Is parameter subdir:$2"
    ##fi

#fi


#if [ $# -gt 1 ]; then
if [ $# -gt 1 ]; then
    if [[ $2 =~ ^-- ]]; then
        # is a flag!
        subdir="NA"
        if [[ -d $rundir/fastq_pass ]]; then
            workdir="$rundir/fastq_pass"
            echo "workdir: $workdir"
        elif [[ -d $rundir/workspace/pass ]]; then
            workdir="$rundir/workspace/pass"
            echo "workdir: $workdir"
        else
            echo "ERROR: No workdir found"
            exit 1
        fi
    else
        # is parameter subdir
        subdir=$2
        workdir="$rundir/$subdir"
        if [[ ! -d $workdir ]]; then echo "ERROR: workdir $workdir does NOT exist. Make sure that subdir $subdir is correct"; exit 1 ; fi
    
    fi
else 
    subdir="NA"
    if [[ -d $rundir/fastq_pass ]]; then
        workdir="$rundir/fastq_pass"
        echo "workdir: $workdir"
    elif [[ -d $rundir/workspace/pass ]]; then
        workdir="$rundir/workspace/pass"
        echo "workdir: $workdir"
    else
        echo "ERROR: No workdir found"
        exit 1
    fi
fi

# >>>>>>>>
#samplesheet="$rundir/samples_v2.tsv"
#fastqdir="$rundir/raw_v2"
#samplesheet="$rundir/samples_guppy6.tsv"
#fastqdir="$rundir/raw_guppy6"
# ---------
samplesheet="$rundir/samples.tsv"
fastqdir="$rundir/raw"
# <<<<<<<<

if [[ -f $samplesheet && $force == false ]];then
    echo "ERROR: Sample sheet $samplesheet already exists and force == false!"
    exit 1
fi



# print parameters

echo "Parameters ------------------------------"
echo
echo "rundir: $rundir"
echo "subdir: $subdir"
echo "samplesheet: $samplesheet"
echo "fastqdir: $fastqdir"
echo
echo "-----------------------------------------"
echo

# ask if in interactive mode
if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        echo "Analysis for samplesheet $samplesheet was aborted by user" | tee $logfile_global
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi




# execution ----------------------------------------

# create dir
mkdir -p $fastqdir

echo -e "sample\tlong\tshort1\tshort2" > $samplesheet


echo "Looping through all barcode dirs"
echo

for barcodedir in $workdir/barcode*; do
    
    barcode_name=`basename $barcodedir`
    #check if in subfolder pass
    if [ -d "$barcodedir/pass" ]; then
        echo "subdir detected"
        barcodedir="$barcodedir/pass"
    else 
        echo "no subdir detected"

    fi

    echo $barcodedir
    fastqgz_count=`ls $barcodedir/*.fastq.gz | wc -l | cut -f 1 -d ' '`  # TODO fix of .gz
    
    echo "fastqgz_count: $fastqgz_count"
# >>>>
#done
#echo "Exit here"
#exit 0
# <<<<

    # check format, ziped or unziped
    fastq_count=`ls $barcodedir/*.fastq | wc -l | cut -f 1 -d ' '`  # TODO fix of .gz
    fastqgz_count=`ls $barcodedir/*.fastq.gz | wc -l | cut -f 1 -d ' '`  # TODO fix of .gz

    if [[ $fastq_count == 0 && $fastqgz_count -gt 0 ]]; then
        echo "Files are compressed"
        compressed=true
        fastq_count=$fastqgz_count
    else
        compressed=false
    fi

    if [[ $fastq_count -gt 1 ]]; then
        echo "barcode data will be merged"
        #barcode_name=`basename $barcodedir` # moved up
        if [[ -f $fastqdir/${barcode_name}.fastq.gz ]]; then
            echo "WARNING: File $fastqdir/${barcode_name}.fastq.gz exists. Skipping ..."
        else 
            if [[ $compressed == true ]]; then
                echo "zcat $barcodedir/*.fastq.gz | gzip > $fastqdir/${barcode_name}.fastq.gz"
                zcat $barcodedir/*.fastq.gz | gzip > $fastqdir/${barcode_name}.fastq.gz
            else
                echo "cat $barcodedir/*.fastq | gzip > $fastqdir/${barcode_name}.fastq.gz"
                cat $barcodedir/*.fastq | gzip > $fastqdir/${barcode_name}.fastq.gz
            fi
        fi
        echo -e "$barcode_name\t$fastqdir/${barcode_name}.fastq.gz\tNA\tNA" >> $samplesheet
        echo "Remove other fastq files???"
    else
        echo "Only single read file; barcode not part of set:"
        #barcode_name=`basename $barcodedir`
        if [[ $compressed == true ]]; then
            single_file=`ls $barcodedir/*.fastq.gz`
        else
            single_file=`ls $barcodedir/*.fastq`
        fi
        readcount=`bioawk -cfastx 'END {print NR}' $single_file`
        echo "barcodedir: $barcodedir with $readcount reads"
        echo -e "$barcode_name\t$single_file\tNA\tNA"
    fi
done


#


echo -e "sample\tlong\tshort1\tshort2" > $samplesheet

for file in $fastqdir/*.fastq.gz; do
    sample=`basename -s .fastq.gz $file`
    echo -e "$sample\t$file\tNA\tNA" >> $samplesheet
done

# compute checksums
for file in $fastqdir/*.fastq.gz; do
    echo "md5sum $file > $file.md5"
    md5sum $file > $file.md5
done



# 
echo "Done parsing barcodes"

echo
echo "Concatenated reads written to $fastqdir"
echo "samplesheet: $samplesheet"
echo

echo "Main output is the sample sheet samples.tsv. It has the following columns:"
echo -e "sample\tlong\tshort1\tshort2"
echo "Use this file and edit samples and short1 short2 prior to using milonga.py"



# prepare sample sheet

#read in sample sheet
#sample barcode
#edit Illumina data by hand?
