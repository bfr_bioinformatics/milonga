# minisnakemake rules for running milong extra modules

assembly="/cephfs/abteilung4/deneke/Projects/milonga_dev/module_testing/testdata/Ec-45.fasta"
SAMPLE="Ec-45"

rule all:
    input:
        expand("{sample}/mash.out", sample=SAMPLE),
        expand("{sample}/mash_sorted.out", sample=SAMPLE),
        expand("{sample}/mash_bestgenus.out", sample=SAMPLE),
        expand("{sample}/checkm/storage/bin_stats_ext.tsv", sample=SAMPLE),


# ---

rule run_mash:
    input: assembly
    output: "{sample}/mash.out"
    message: "Running mash"
    threads:10
    params:
        #mashdb = "/home/DenekeC/Snakefiles/aquamis/reference_db/mash/refseq_bacteria_viruses_plasmids.k21s1000.msh"
        mashdb = "/home/DenekeC/Snakefiles/aquamis/reference_db/confindr/refseq.msh"
    shell:
        """
        echo "mash dist {params.mashdb} {input} > {output}"
        mash dist {params.mashdb} {input} > {output}
        """
        
rule sort_mash:
    input: "{sample}/mash.out"
    output: "{sample}/mash_sorted.out"
    message: "Parsing mash"
    threads:1
    params:
        min_mashdist = 400
    shell:
        """
        cat {input}  | awk 'BEGIN {{OFS="\t"}}; {{split($5,a,"/"); print $1, $2, $3, $4, a[1], a[2]}}' | sort -nr -k 5 > {output}
        """
        # | head -n 1  # makes problems


rule best_genus:
    input: "{sample}/mash_sorted.out"
    output: "{sample}/mash_bestgenus.out"
    message: "Parsing mash"
    threads:1
    params:
        min_mashdist = 400,
        parse_level = "genus" # col 11
        #parse_level = "species" # col 12
    shell:
        """
        best_mash_dist=`head -n 1 {input} | cut -f 5`
        if [[ $best_mash_dist -le {params.min_mashdist} ]]; then
            echo "ERROR. Genus cannot be accuratly determined. Mash score $best_mash_dist is lower than {params.min_mashdist}!. Exit"
            exit 1
        else
        echo "Mash is alright"       
            head -n 1 {input} | cut -f 1 -d $'\t' | cut -f 11 -d '/' > {output}
        fi
        """
        


#rule test_if_exist_best_reference_genus:
    #input: {assembly}
    #output: {sample}/mash.out
    #message:
    #threads:1
    #params:
        #mashdb = ""
    #shell:
        #"mash"


rule copy_assembly:
    input: 
        assembly = assembly,
    output:
        "{sample}/tmp/{sample}.fasta"
    message: "Copy assembly"
    shell:
        "cp {input} {output}"

rule run_checkm:
    input: 
        #assembly = {assembly},
        assembly = "{sample}/tmp/{sample}.fasta",
        mash = "{sample}/mash_bestgenus.out"
    output: "{sample}/checkm/storage/bin_stats_ext.tsv"
    message: "run checkm"
    threads:10
    log: "logs/checkm_{sample}.log"
    params:
        outdir = "{sample}/checkm",
        assemblydir= "{sample}/tmp"
        #assemblydir= "{wildcards.sample}/tmp"
    shell:
        """
        best_genus=`cat {input.mash}`
        #assemblydir=`dirname {input.assembly}`
        echo "checkm taxonomy_wf -x fasta -a {params.outdir}/alignment_file.txt genus \"${{best_genus}}\" {params.assemblydir} {params.outdir}"
        #echo "checkm taxonomy_wf -x fasta -a {params.outdir}/alignment_file.txt genus \"${{best_genus}}\" $assemblydir {params.outdir}"
        checkm taxonomy_wf -x fasta -a {params.outdir}/alignment_file.txt genus "${{best_genus}}" {params.assemblydir} {params.outdir} &> {log}
        """
        
        
        
#        "checkm taxonomy_wf -x fasta -a ./taxonomy_wf_genus/alignment_file.txt genus "Escherichia" ../testdata/ ./taxonomy_wf_genus"



#
#rule run_mash:
    #input: {assembly}
    #output: {sample}/mash.out
    #message:
    #threads:1
    #params:
        #mashdb = ""
    #shell:
        #"mash"
