# extra rules

rule run_kraken2_contigs:
    input:
        final_assembly = "{sample}/{assembler}/{sample}.fasta"
    output:
        kraken_out ="{sample}/{assembler}/kraken2/{sample}.fromcontigs.kraken",
        kraken_report="{sample}/{assembler}/kraken2/{sample}.fromcontigs.kreport"
    message: "Running kraken2 on {wildcards.sample} for {wildcards.assembler}"
    log:
       "logs/kraken2_{assembler}_{sample}.log"
    params:
        kraken2_db=config["params"]["kraken2"]["db_kraken"],
    threads:
        config["params"]["threads"]
    shell:
        """
        kraken2 --db {params.kraken2_db} --threads {threads} --output {output.kraken_out} --report {output.kraken_report} {input}
        """

rule run_taxonkits_contigs:
    input:
        kraken_out ="{sample}/{assembler}/kraken2/{sample}.fromcontigs.kraken"
    params:
        taxondb=config["params"]["kraken2"]["taxonkit_db"]
    output:
        taxonkit_tmp=temp("{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit.tmp"),
        taxonkit_out="{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit"
    message:
        "Running TaxonKit on Contigs of {wildcards.sample} for {wildcards.assembler}"
    threads:
        config["params"]["threads"]
    shell:
        """
        cut -f 3 {input} | taxonkit lineage --data-dir {params.taxondb} | taxonkit reformat --data-dir {params.taxondb} --format "k__{{k}}|p__{{p}}|c__{{c}}|o__{{o}}|f__{{f}}|g__{{g}}|s__{{s}}" --miss-rank-repl "unclassified" | cut -f 3 > {output.taxonkit_tmp}
        awk 'BEGIN {{ FS = "\t" }} ; FNR==NR{{ a[FNR""] = $2 FS $3 FS $4; next }}{{ print a[FNR""] FS $0 }}' {input} {output.taxonkit_tmp} > {output.taxonkit_out}
        """

rule parse_kraken2_contigs:
    input:
        taxonkit_out="{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit"
    output:
        summary = "{sample}/{assembler}/kraken2/{sample}.kraken.fromcontigs.summary.tsv"
    message: "Parsing kraken2 contigs results for {wildcards.sample} and {wildcards.assembler}"
    params:
        taxlevel = config["params"]["kraken2"]["taxonkit_taxlevel"] # species or genus
    script:
        "../scripts/parse_kraken_contigs.R"


rule copy_assembly:
    input: 
        final_assembly = "{sample}/{assembler}/{sample}.fasta"
    output:
        assembly_copy = "{sample}/{assembler}/checkm/tmp/{sample}.fasta"
        #temporary("{sample}/{assembler}/checkm/tmp/{sample}.fasta") # make this temporary
    message: "Copy assembly"
    shell:
        "cp {input} {output}"



rule bin_assembly:
    input: 
        final_assembly = "{sample}/{assembler}/{sample}.fasta",
        taxonkit_out="{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit"
    output:
        #directory("{sample}/{assembler}/checkm/genome_bins")
        "{sample}/{assembler}/checkm/genome_bins"
    message: "bin assembly"
    params:
        taxlevel = "species",
        species_list = "{sample}/{assembler}/checkm/genome_bins/species.list"
    shell:
        """
        awk -F 's__' '{print $2}' {input.taxonkit_out} | sort | uniq > {params.species_list}
        cat {params.species_list} | while read species; do
            echo $species
            species_clean=`echo $species | tr ' ' '_'`
            contiglist=`grep "s__$species" {input.taxonkit_out} | cut -f 1`
            # loop through contiglist
            for mycontig in $contiglist; do 
                echo $mycontig
                bioawk -c fastx -v mycontig=$mycontig '{{if($name == mycontig) print ">"$name" "$comment"\n"$seq}}' {input.final_assembly} > {output}/${species_clean}.fasta
            done
        done
        """


#rule run_checkm:
    #input: 
        ##assembly = {assembly},
        #assembly = "{sample}/tmp/{sample}.fasta",
        #mash = "{sample}/mash_bestgenus.out"
    #output: "{sample}/checkm/storage/bin_stats_ext.tsv"
    #message: "run checkm"
    #threads:10
    #log: "logs/checkm_{sample}.log"
    #params:
        #outdir = "{sample}/checkm",
        #assemblydir= "{sample}/tmp"



rule run_checkm:
    input:
        final_assembly = "{sample}/{assembler}/checkm/tmp/{sample}.fasta"
    output:
        checkm_table = "{sample}/{assembler}/checkm/results_checkM_genome.tsv"
    message: "Running checkm on {wildcards.sample} for {wildcards.assembler}"
    log:
       "logs/checkm_{assembler}_{sample}.log"
    params:
        outdir = "{sample}/{assembler}/checkm",
        assemblydir= "{sample}/{assembler}/checkm/tmp"
    threads:
        config["params"]["threads"]
    shell:
        "checkm lineage_wf --tab_table -f {output.checkm_table} -t {threads} -x fasta {params.assemblydir} {params.outdir}"
        

rule refine_checkm:
    input:
        checkm_table = "{sample}/{assembler}/checkm/results_checkM_genome.tsv"
    output:
        checkm_table = "{sample}/{assembler}/checkm/results_checkM_tree_qa_genome.tsv"
    message: "Running checkm on {wildcards.sample} for {wildcards.assembler}"
    log:
       "logs/checkm_tree_qa_{assembler}_{sample}.log"
    params:
        outdir = "{sample}/{assembler}/checkm"
    threads:
        config["params"]["threads"]
    shell:
        "checkm tree_qa --tab_table -f {output.checkm_table} {params.outdir}"



rule run_checkm_taxonomy:
    input: 
        final_assembly = "{sample}/{assembler}/checkm/tmp/{sample}.fasta",
        krakensummary = "{sample}/{assembler}/kraken2/{sample}.kraken.fromcontigs.summary.tsv"
    output: 
        checkm_table = "{sample}/{assembler}/checkm_taxonomy/results_checkM_genome.tsv"
    message: "Running checkm_taxonomy on {wildcards.sample} for {wildcards.assembler}"
    log:
       "logs/checkm_taxonomy_{assembler}_{sample}.log"
    params:
        outdir = "{sample}/{assembler}/checkm_taxonomy",
        assemblydir= "{sample}/{assembler}/checkm/tmp",
        taxlevel = "species"
    threads:
        config["params"]["threads"]
    shell:
        """
        if [[ {params.taxlevel} == "species" ]]; then
            echo "taxlevel is species"
            best_species=`cut -f 1 {input.krakensummary} | tail -n +2  | head -n 1`
        elif [[ {params.taxlevel} == "genus" ]]; then
            echo "taxlevel is genus"
            best_species=`cut -f 1 {input.krakensummary} | tail -n +2  | head -n 1` | cut -f 1 -d ' '
        else
            echo "Error: taxlevel must be species or genus"
            exit 1
        fi
        # TODO check if species in allowed set
        echo "checkm taxonomy_wf -x fasta --tab_table -f {output.checkm_table}  -t {threads} {params.taxlevel} "${{best_species}}" {params.assemblydir} {params.outdir}"
        checkm taxonomy_wf -x fasta --tab_table -f {output.checkm_table}  -t {threads} {params.taxlevel} "${{best_species}}" {params.assemblydir} {params.outdir} &> {log}
        """



rule self_alignment:
    input:
        final_assembly = "{sample}/{assembler}/{sample}.fasta"
    output:
        self_alignment = "{sample}/{assembler}/selfalignment/{sample}.paf"
    message: "Running minimap selfalignment on {wildcards.sample} for {wildcards.assembler}"
    log:
       "logs/selfalignment_{assembler}_{sample}.log"
    threads:
        config["params"]["threads"]
    shell:
        "minimap2 -x asm5 {input} {input} > {output}"

# not working! make this part of report?!
#rule plot_self_alignment:
    #input:
        #self_alignment = "{sample}/{assembler}/selfalignment/{sample}.paf"
    #output:
        #self_alignment = "{sample}/{assembler}/selfalignment/{sample}.html"
    #message: "Running plot selfalignment on {wildcards.sample} for {wildcards.assembler}"
    #log:
       #"logs/plot_selfalignment_{assembler}_{sample}.log"
    #threads:
        #config["params"]["threads"]
    #params:
        #prefix = working_dir + "{sample}/{assembler}/selfalignment/{sample}"
    #shell:
        #"/home/DenekeC/software/dotPlotly/pafCoordsDotPlotly.R -i {input} -o {params.prefix} -s -t -m 500 -q 100 -l"


rule parse_self_alignment:
    input:
        self_alignment = "{sample}/{assembler}/selfalignment/{sample}.paf"
    output:
        filtered_alignment =  "{sample}/{assembler}/selfalignment/{sample}_filtered.paf",
        selfalignment_summary = "{sample}/{assembler}/selfalignment/{sample}.summary.tsv"
    message: "Parse selfalignment on {wildcards.sample} for {wildcards.assembler}"
    params:
        sample = "{sample}", #"{wildcards.sample}",
        assembly =  "{assembler}" # "{wildcards.assembler}"
    script:
        "../scripts/parse_selfalignment.R"
        


rule unicycler_flye_alignment:
    input:
        flye_assembly = "{sample}/flye/{sample}.fasta",
        unicycler_assembly = "{sample}/unicycler/{sample}.fasta"
    output:
        self_alignment = "{sample}/assembly_alignment/{sample}_flye_vs_unicycler.paf"
    message: "Running minimap on {wildcards.sample} for flye vs. uniycler"
    log:
       "logs/assemblyalignment_{sample}.log"
    threads:
        config["params"]["threads"]
    shell:
        "minimap2 -x asm5 {input.unicycler_assembly} {input.flye_assembly} > {output}"
        
        
#"minimap2 -x asm5 {input.flye_assembly} {input.unicycler_assembly} > {output}"
        
# not working! make this part of report?!
#rule plot_unicycler_flye_alignment:

# GOAL:
# identify contigs
# find duplications, missing parts    
  
# rule parse_unicycler_flye_alignment:
    # input:
        # self_alignment = "{sample}/assembly_alignment/{sample}.paf"
    # output:
        # self_alignment = "{sample}/assembly_alignment/summary.tsv"
    # message: "Parse minimap on {wildcards.sample} for flye vs. uniycler"
    # script:
        # ""
    

