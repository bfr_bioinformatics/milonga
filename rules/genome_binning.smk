# rules for genome binning

import os
import pandas as pd
#import json
#import csv
# import shutil #needed to check if programs exists
# from snakemake.utils import R
shell.executable("bash")


# definitions ---------------------------------------------------------------

# Set snakemake main workdir variable
#workdir: config["workdir"]
#workdir: "/cephfs/abteilung4/deneke/Projects/milonga_dev/module_testing/genome_binning"
#assembly="/cephfs/abteilung4/deneke/Projects/milonga_dev/testdata/metagenomic_example/21-EC00618.fasta"
#Sample="21-EC00618"


workdir: config["workdir"]

#workdir: "/cephfs/abteilung4/deneke/Projects/milonga_dev/module_testing/genome_binning_2"
#assembly="/cephfs/abteilung4/Datentransfer/MinIon_Network/MinIon_20201001/feces_mixed_MC/flye/feces_mixed_MC.fasta"
#assembly="/cephfs/abteilung4/Datentransfer/MinIon_Network/MinIon_20201023/water_fixed_MC/flye/water_fixed_MC.fasta"

samples = pd.read_table(config["samples"], index_col="sample")
samples.index = samples.index.astype('str', copy=False) # in case samples are integers, need to convert them to str


print(samples)

#Sample="water_fixed_MC"
#sSample="feces_mixed_MC"

Assembler="flye"


rule all:
    input:
        expand("{sample}/{assembler}/checkm_binned/genome_bins/.finished", sample=samples.index, assembler=Assembler),
        expand("{sample}/{assembler}/checkm_binned/results_checkM_genome.tsv", sample=samples.index, assembler=Assembler),



# skip this when milinga already invoked
#rule run_kraken2_contigs:
    #input:
        #final_assembly = assembly
    #output:
        #kraken_out ="{sample}/{assembler}/kraken2/{sample}.fromcontigs.kraken",
        #kraken_report="{sample}/{assembler}/kraken2/{sample}.fromcontigs.kreport"
    #message: "Running kraken2 on {wildcards.sample} for {wildcards.assembler}"
    #log:
       #"logs/kraken2_{assembler}_{sample}.log"
    #params:
        #kraken2_db=config["params"]["kraken2"]["db_kraken"],
    #threads:
        #config["params"]["threads"]
    #shell:
        #"""
        #kraken2 --db {params.kraken2_db} --threads {threads} --output {output.kraken_out} --report {output.kraken_report} {input}
        #"""

rule run_taxonkits_contigs:
    input:
        kraken_out ="{sample}/{assembler}/kraken2/{sample}.fromcontigs.kraken"
    params:
        taxondb=config["params"]["kraken2"]["taxonkit_db"]
    output:
        taxonkit_tmp=temp("{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit.tmp"),
        taxonkit_out="{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit"
    message:
        "Running TaxonKit on Contigs of {wildcards.sample} for {wildcards.assembler}"
    threads:
        config["params"]["threads"]
    shell:
        """
        cut -f 3 {input} | taxonkit lineage --data-dir {params.taxondb} | taxonkit reformat --data-dir {params.taxondb} --format "k__{{k}}|p__{{p}}|c__{{c}}|o__{{o}}|f__{{f}}|g__{{g}}|s__{{s}}" --miss-rank-repl "unclassified" | cut -f 3 > {output.taxonkit_tmp}
        awk 'BEGIN {{ FS = "\t" }} ; FNR==NR{{ a[FNR""] = $2 FS $3 FS $4; next }}{{ print a[FNR""] FS $0 }}' {input} {output.taxonkit_tmp} > {output.taxonkit_out}
        """

rule parse_kraken2_contigs:
    input:
        taxonkit_out="{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit"
    output:
        summary = "{sample}/{assembler}/kraken2/{sample}.kraken.fromcontigs.summary.tsv"
    message: "Parsing kraken2 contigs results for {wildcards.sample} and {wildcards.assembler}"
    params:
        taxlevel = config["params"]["kraken2"]["taxonkit_taxlevel"] # species or genus
    script:
        "../scripts/parse_kraken_contigs.R"



# TODO skip undetermined
# TODO remove small contigs, small total length per unit

rule bin_assembly:
    input: 
        #final_assembly = assembly, # Only when invoked separatly and by assembly
        final_assembly = "{sample}/flye/assembly.fasta",
        taxonkit_out="{sample}/{assembler}/kraken2/{sample}.fromcontigs.taxonkit"
    output:
        #directory("{sample}/{assembler}/checkm_binned/genome_bins")
        #"{sample}/{assembler}/checkm_binned/genome_bins"
        finished="{sample}/{assembler}/checkm_binned/genome_bins/.finished"
    message: "bin assembly for sample {wildcards.sample} for {wildcards.assembler}"
    params:
        taxlevel = "species", # "genus"
        species_list = "{sample}/{assembler}/checkm_binned/genome_bins/species.list",
        outdir = "{sample}/{assembler}/checkm_binned/genome_bins"
    shell:
        """
        mkdir -p {params.outdir}
        awk -F 's__' '{{print $2}}' {input.taxonkit_out} | sort | uniq > {params.species_list}
        cat {params.species_list} | while read species; do
            echo $species
            speciesclean=`echo $species | tr ' ' '_' | tr -d '[' | tr -d ']'`
            echo $speciesclean
            contiglist=`grep -F "s__${{species}}" {input.taxonkit_out} | cut -f 1`
            echo "contiglist: $contiglist"
            # loop through contiglist
            for mycontig in $contiglist; do 
                echo $mycontig
                bioawk -c fastx -v mycontig=$mycontig '{{if($name == mycontig) print ">"$name" "$comment"\\n"$seq}}' {input.final_assembly} >> {params.outdir}/$speciesclean.fasta
            done # end for
        done # end while
        touch {output.finished}
        """


rule run_checkm:
    input:
        #final_assembly = "{sample}/{assembler}/checkm_binned/tmp/{sample}.fasta",
        finished="{sample}/{assembler}/checkm_binned/genome_bins/.finished"
    output:
        checkm_table = "{sample}/{assembler}/checkm_binned/results_checkM_genome.tsv"
    message: "Running checkm on {wildcards.sample} for {wildcards.assembler}"
    log:
       "logs/checkm_{assembler}_{sample}.log"
    params:
        outdir = "{sample}/{assembler}/checkm",
        assemblydir= "{sample}/{assembler}/checkm_binned/genome_bins"
    threads:
        config["params"]["threads"]
    shell:
        "checkm lineage_wf --tab_table -f {output.checkm_table} -t {threads} -x fasta {params.assemblydir} {params.outdir}"




                #grep $mycontig {input.final_assembly} >> {params.outdir}/$speciesclean.fasta
                #bioawk -c fastx -v mycontig=$mycontig '{{if($name == mycontig) print ">"$name" "$comment"\n"$seq}}' {input.final_assembly} >> {params.outdir}/$speciesclean.fasta

                #echo "bioawk -c fastx -v mycontig=$mycontig '{{if($name == mycontig) print \">\"$name\" \"$comment\"\n\"$seq}}' {input.final_assembly} >> {output}/$speciesclean.fasta"
                #bioawk -c fastx -v mycontig=$mycontig '{{if($name == mycontig) print ">"$name" "$comment"\n"$seq}}' {input.final_assembly} >> {output}/$speciesclean.fasta
