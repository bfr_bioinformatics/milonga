# pilon

# Snakemake pipeline for long read and hybrid assembly

import os
import pandas as pd
#import json
#import csv
# import shutil #needed to check if programs exists
# from snakemake.utils import R
shell.executable("bash")


# definitions ---------------------------------------------------------------

# Set snakemake main workdir variable
#workdir: config["workdir"]

workdir: "/home/DenekeC/Snakefiles/long_read_assembly_pipeline/testdata/pilon_test"
sample_file = "/home/DenekeC/Snakefiles/long_read_assembly_pipeline/testdata/samples.tsv"



# samples
samples = pd.read_table(sample_file, index_col="sample")
#samples = pd.read_table(config["samples"], index_col="sample")
samples.index = samples.index.astype('str', copy=False) # in case samples are integers, need to convert them to str



num_pilon_iterations = 3
print(num_pilon_iterations)
print(type(num_pilon_iterations))

# all rules

rule all:
    input:
        expand("{sample}/flye/pilon/iteration{n}/assembly.polished.fasta", sample=samples.index,n = num_pilon_iterations),
        expand("{sample}/flye/pilon/assembly.pilon.fasta", sample=samples.index)

# functions

def _get_fastq(wildcards,read_pair='fq1'):
    return samples.loc[(wildcards.sample), [read_pair]].dropna()[0]


def recurse_sample(wcs):
    n = int(wcs.n)
    #n = wcs.n
    print("n:" + wcs.n)
    if n > 0:
        return "%s/flye/pilon/iteration%d/assembly.fasta" % (wcs.sample,n)
    #if n == 1:
        #return "%s/flye/assembly.fasta" % wcs.sample
    #elif n > 1:
        #return "%s/flye/pilon/iteration%d/assembly.fasta" % (wcs.sample,n-1)
    else:
        raise ValueError("loop numbers must be 1 or greater: received %s" % wcs.n)


def recurse_sample2(wcs):
    m = int(wcs.m)
    if m > 0:
        return "%s/flye/pilon/iteration%d/assembly.polished.fasta" % (wcs.sample,m-1) # m-1
    else:
        raise ValueError("loop numbers must be 0 or greater: received %s" % wcs.m)


# rules


#rule initial_copy:
    #input: 
        #"{sample}/flye/assembly.fasta"
    #output:
        #"{sample}/flye/pilon/iteration1/assembly.fasta"
    #message: "initial copy for pilon"
    #shell:
        #"cp {input} {output}"


rule initial_copy:
    input: 
        "{sample}/flye/assembly.fasta"
    output:
        temporary("{sample}/flye/pilon/iteration0/assembly.polished.fasta")
    message: "initial copy for pilon"
    shell:
        "cp {input} {output}"


rule final_copy:
    input: 
        expand("{{sample}}/flye/pilon/iteration{n}/assembly.polished.fasta",n=num_pilon_iterations)
    output:
        "{sample}/flye/pilon/assembly.pilon.fasta"
    message: "initial copy for pilon"
    shell:
        "cp {input} {output}"




rule pilon_index_contigs:
    input: 
        flye_assembly = recurse_sample,
    output: 
        temporary("{sample}/flye/pilon/iteration{n}/assembly.fasta.1.bt2"),
        temporary("{sample}/flye/pilon/iteration{n}/assembly.fasta.2.bt2"),
        temporary("{sample}/flye/pilon/iteration{n}/assembly.fasta.3.bt2"),
        temporary("{sample}/flye/pilon/iteration{n}/assembly.fasta.4.bt2"),
        temporary("{sample}/flye/pilon/iteration{n}/assembly.fasta.rev.1.bt2"),
        temporary("{sample}/flye/pilon/iteration{n}/assembly.fasta.rev.2.bt2"),
    wildcard_constraints:
        sample="[^/]+",
        n="[0-9]+"
    shell:
        """
        bowtie2-build {input} {input}
        #touch {output}
        """
        #echo "pilon {input} > {output}"
        #echo "{input},{output}" > {output}
#cp {input} {output}

rule pilon_map_contigs:
    input: 
        flye_assembly = "{sample}/flye/pilon/iteration{n}/assembly.fasta",
        flye_assembly_index = "{sample}/flye/pilon/iteration{n}/assembly.fasta.1.bt2",
        short1 = lambda wildcards: _get_fastq(wildcards, 'short1'),
        short2 = lambda wildcards: _get_fastq(wildcards, 'short2')
    output:
        pilon_mapping = temporary("{sample}/flye/pilon/iteration{n}/shortreads2assembly.bam"), 
    message: "pilon polish flye for sample {wildcards.sample} round {wildcards.n}"
    log: "logs/pilon_map_contigs_{sample}_iteration{n}.log"
    threads: 10
    shell:
        """
        echo "bowtie2 --local --very-sensitive-local --threads {threads} -I 221 -X 872 -x {input.flye_assembly} -1 {input.short1} -2  {input.short2} | samtools view  --threads {threads} -S -b -u - | samtools sort - -o {output} --threads {threads} &> {log}"
        bowtie2 --local --very-sensitive-local --threads {threads} -I 221 -X 872 -x {input.flye_assembly} -1 {input.short1} -2  {input.short2} | samtools view  --threads {threads} -S -b -u - | samtools sort - -o {output} --threads {threads} &>> {log}
        #touch {output}
        """    


rule pilon_index_bam:
    input: 
        pilon_mapping = "{sample}/flye/pilon/iteration{n}/shortreads2assembly.bam", 
    output:
        pilon_mapping_index = temporary("{sample}/flye/pilon/iteration{n}/shortreads2assembly.bam.bai"), 
    message: "pilon index mapping for sample {wildcards.sample}"
    threads: 10
    shell:
        """
        echo "samtools index {input}"
        samtools index {input}
        #touch {output}
        """    


rule pilon_run:
    input: 
        pilon_mapping = "{sample}/flye/pilon/iteration{n}/shortreads2assembly.bam", 
        flye_assembly = "{sample}/flye/pilon/iteration{n}/assembly.fasta",
        pilon_mapping_index = "{sample}/flye/pilon/iteration{n}/shortreads2assembly.bam.bai", 
    output:
        #flye_polished = "{sample}/flye/pilon/assembly.fasta",
        flye_polished = temporary("{sample}/flye/pilon/iteration{n}/assembly.polished.fasta"),
        changes = "{sample}/flye/pilon/iteration{n}/assembly.polished.changes"
        #changes = "{sample}/flye/pilon/assembly.changes"
    message: "pilon polish flye for sample {wildcards.sample}"
    threads: 10
    params:
        outdir = "{sample}/flye/pilon/iteration{n}",
        extra = "", # extra parameters passed to pilon
        fix = "all"
    shell:
        """
         echo "pilon {params.extra} --genome {input.flye_assembly} --changes --output assembly.polished --outdir {params.outdir} --fix bases --frags {input.pilon_mapping}"
         pilon {params.extra} --genome {input.flye_assembly} --changes --output assembly.polished --outdir {params.outdir} --fix {params.fix} --frags {input.pilon_mapping}
         #touch {output.flye_polished}
         #touch {output.changes}
        """    


rule restart_next_iteration:
    input: recurse_sample2
    output: temporary("{sample}/flye/pilon/iteration{m}/assembly.fasta")
    wildcard_constraints:
        sample="[^/]+",
        m="[0-9]+"
    shell:
        """
        echo "copy input {input} > {output}"
        cp {input} {output}
        """
